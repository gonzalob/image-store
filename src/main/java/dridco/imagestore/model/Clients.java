package dridco.imagestore.model;

import com.google.common.base.Optional;

import java.util.Collection;

public interface Clients {

	Optional<Client> get(String clientKey);

	Collection<Client> getAll();
}
