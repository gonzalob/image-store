package dridco.imagestore.model;

import java.util.Date;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class Release implements Command<Boolean> {

	private String identifier;
	private StorageEventListener listener;
	private Hash hash;

	public Release(String identifier, Hash hash) {
		this(identifier, hash, new StorageEventListenerTemplate());
	}

	public Release(String identifier, Hash hash, StorageEventListener listener) {
		this.identifier = identifier;
		this.hash = hash;
		this.listener = listener;
	}

	@Override
	public Boolean execute(Identifiers identifiers, Images images, Date when) throws HashMismatch, IdentifiersException {
		Identifier stored = identifiers.get(identifier);
		Image image = images.get(stored);
		if(hash.matches(image)) {
			return remove(stored, image, identifiers, images);
		} else {
			throw new HashMismatch();
		}
	}

	private Boolean remove(Identifier identifier, Image image, Identifiers identifiers, Images images) throws IdentifiersException {
		Reachability state = identifiers.remove(identifier);
		listener.released(identifier);
		if (state.remove(images)) {
			listener.removed(image);
			return TRUE;
		} else return FALSE;
	}
}
