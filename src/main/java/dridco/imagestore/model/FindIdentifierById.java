package dridco.imagestore.model;

import com.google.common.base.Predicate;

public class FindIdentifierById implements Predicate<Identifier> {

	public static FindIdentifierById identifiedBy(String id) {
		return new FindIdentifierById(id);
	}

	private String id;

	public FindIdentifierById(String id) {
		this.id = id;
	}

	@Override
	public boolean apply(Identifier input) {
		return input.identifiedBy(id);
	}
}
