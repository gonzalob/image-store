package dridco.imagestore.model.gcs;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import dridco.imagestore.model.*;

public class GoogleCloudStorageImages implements Images {

    private Bucket bucket;

    public GoogleCloudStorageImages(StorageOptions options, String bucketName) {
        Storage service = options.getService();
        bucket = service.get(bucketName);
    }

    @Override
    public Image get(final Identifier identifier) {
        Optional<Blob> blob = getByIdentifier(identifier);
        return blob.transform(new Function<Blob, Image>() {

            @Override
            public Image apply(Blob input) {
                return new RawImage(identifier, input.getContent());
            }
        }).or(
                new NoContent(
                        identifier,
                        new ImageStoreException("Image with hash " + identifier.getHash() + " not found"))
        );
    }

    @Override
    public boolean store(RawImage created) {
        Identifier identifier = created.getIdentifier();
        if (getByIdentifier(identifier).isPresent()) {
            return false;
        } else {
            bucket.create(identifier.getHash(), created.getContent());
            return true;
        }
    }

    @Override
    public void remove(String hash) {
        Optional<Blob> blob = getByHash(hash);
        if (blob.isPresent()) {
            blob.get().delete();
        }
    }

    @Override
    public Integer count() {
        return Iterables.size(bucket.list().getValues());
    }

    private Optional<Blob> getByIdentifier(Identifier identifier) {
        return getByHash(identifier.getHash());
    }

    private Optional<Blob> getByHash(String hash) {
        return Optional.fromNullable(bucket.get(hash));
    }

    void clear() {
        for (Blob blob : bucket.list().getValues()) {
            blob.delete();
        }
    }
}
