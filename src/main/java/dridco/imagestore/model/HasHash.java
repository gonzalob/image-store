package dridco.imagestore.model;

import com.google.common.base.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;

public class HasHash implements Predicate<Image> {

	public static HasHash hasHash(String hash) {
		return new HasHash(hash);
	}

	private String hash;

	public HasHash(String hash) {
		this.hash = checkNotNull(hash);
	}

	@Override
	public boolean apply(Image input) {
		return hash.equals(input.getIdentifier().getHash());
	}
}
