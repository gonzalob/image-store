package dridco.imagestore.model;

import static com.google.common.base.Preconditions.checkNotNull;

public class Storage {

	private Identifiers identifiers;
	private Images images;
	private Clock clock;

	public Storage(Identifiers identifiers, Images images, Clock clock) {
		this.identifiers = checkNotNull(identifiers);
		this.images = checkNotNull(images);
		this.clock = clock;
	}

	public boolean contains(String id) throws IdentifiersException {
		return identifiers.contains(id);
	}

	public <T> T execute(Command<T> command) throws ImageStoreException {
		return command.execute(identifiers, images, clock.now());
	}
}
