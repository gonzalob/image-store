package dridco.imagestore.model.inmemory;

import dridco.imagestore.model.Identifier;

import java.util.Date;

import static dridco.imagestore.model.Dates.copy;

public class DueGracePeriodPredicate extends DueDatePredicate {

	public static DueGracePeriodPredicate gracePeriodExpired(Date when) {
		return new DueGracePeriodPredicate(when);
	}

	private Date dueDate;

	public DueGracePeriodPredicate(Date when) {
		this.dueDate = copy(when);
	}

	@Override
	protected Date identifierDate(Identifier identifier) {
		return identifier.getCreated();
	}

	@Override
	protected Date dueDate() {
		return dueDate;
	}
}
