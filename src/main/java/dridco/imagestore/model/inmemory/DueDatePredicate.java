package dridco.imagestore.model.inmemory;

import com.google.common.base.Predicate;
import dridco.imagestore.model.Identifier;

import java.util.Date;

public abstract class DueDatePredicate implements Predicate<Identifier> {

	protected abstract Date identifierDate(Identifier identifier);

	protected abstract Date dueDate(); 

	@Override
	public final boolean apply(Identifier input) {
		return identifierDate(input).before(dueDate());
	}

}
