package dridco.imagestore.model.inmemory;

import com.google.common.collect.Maps;
import dridco.imagestore.model.*;

import java.util.Map;

public class InMemoryImages implements Images {

	private Map<String, byte[]> images = Maps.newConcurrentMap();

	@Override
	public Image get(Identifier identifier) {
		try {
			byte[] content = images.get(identifier.getHash());
			return new RawImage(identifier, content);
		} catch(NullPointerException e) {
			return new NoContent(identifier, e);
		}
	}

	@Override
	public boolean store(RawImage created) {
		String hash = created.getIdentifier().getHash();
		if(!images.containsKey(hash)) {
			images.put(hash, created.getContent());
			return true;
		}
		return false;
	}

	@Override
	public void remove(String hash) {
		images.remove(hash);
	}

	@Override
	public Integer count() {
		return images.size();
	}
}
