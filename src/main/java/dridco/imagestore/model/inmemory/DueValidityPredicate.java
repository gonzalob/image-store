package dridco.imagestore.model.inmemory;

import dridco.imagestore.model.Identifier;

import java.util.Date;

import static dridco.imagestore.model.Dates.copy;

public class DueValidityPredicate extends DueDatePredicate {

	public static DueValidityPredicate validationExpired(Date when) {
		return new DueValidityPredicate(when);
	}

	private Date dueDate;

	public DueValidityPredicate(Date when) {
		this.dueDate = copy(when);
	}

	@Override
	protected Date identifierDate(Identifier identifier) {
		return identifier.getLastChecked();
	}

	@Override
	protected Date dueDate() {
		return dueDate;
	}
}
