package dridco.imagestore.model.inmemory;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import dridco.imagestore.model.*;

import java.util.List;

import static com.google.common.collect.Iterables.*;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static dridco.imagestore.model.FindIdentifierById.identifiedBy;
import static dridco.imagestore.model.FindIdentifierWithHash.linkingTo;
import static dridco.imagestore.model.inmemory.BelongingToPredicate.belongingTo;
import static dridco.imagestore.model.inmemory.DueGracePeriodPredicate.gracePeriodExpired;
import static dridco.imagestore.model.inmemory.DueValidityPredicate.validationExpired;
import static dridco.imagestore.model.inmemory.NotLockedPredicate.notLocked;
import static java.util.Arrays.asList;

public class InMemoryIdentifiers implements Identifiers {

	private List<Identifier> identifiers = newArrayList();
	private List<Identifier> deleted = newArrayList();
	private List<Identifier> releasing = newArrayList();

	@Override
	public boolean contains(final String id) {
		return findIfExists(id).isPresent();
	}

	@Override
	public Identifier get(String id) throws IdentifiersException {
		Optional<Identifier> candidate = findIfExists(id);
		if (!findInDeleted(id) && candidate.isPresent()) {
			return candidate.get();
		} else {
			throw cantGet(id);
		}
	}

	@Override
	public void store(Identifier identifier) throws IdentifiersException {
		String id = identifier.toString();
        if(findInDeleted(id)) {
            throw new IdentifierDeleted();
        }
		if(contains(id)) {
			throw new IdentifierExists();
		}
		identifiers.add(identifier);
	}

	@Override
	public Reachability remove(Identifier identifier) throws IdentifierDoesNotExist {
		if (identifiers.remove(identifier)) {
			deleted.add(identifier);
		} else {
			throw new IdentifierDoesNotExist();
		}
		if (any(identifiers, linkingTo(identifier.getHash()))) {
			return stillLinked();
		} else {
			return orphaned(identifier);
		}
	}

	private IdentifiersException cantGet(String id) throws IdentifierDoesNotExist, IdentifierDeleted {
		if(findInDeleted(id)) {
			throw new IdentifierDeleted();
		} else {
			throw new IdentifierDoesNotExist();
		}
	}

	private Reachability orphaned(Identifier identifier) {
		return new Orphaned(identifier);
	}

	private Reachability stillLinked() {
		return new StillLinked();
	}

	private Optional<Identifier> findIfExists(String id) {
		return findInList(id, identifiers);
	}

	private boolean findInDeleted(String id) {
		return findInList(id, deleted).isPresent();
	}

	private Optional<Identifier> findInList(String id, Iterable<Identifier> list) {
		return tryFind(list, identifiedBy(id));
	}

	@Override
	public Iterable<Identifier> candidatesToRelease(CleanupRequest collector) {
		Iterable<Identifier> ownedByClient = filter(identifiers, belongingTo(collector.getOwner()));
		Iterable<Identifier> gracePeriodExpired = filter(ownedByClient, gracePeriodExpired(collector.getDueGracePeriod()));
		Iterable<Identifier> expired = filter(gracePeriodExpired, validationExpired(collector.getDueValidity()));
		Iterable<Identifier> notLocked = filter(expired, notLocked(releasing));
		Iterables.addAll(releasing, notLocked);
		return unmodifiableIterable(newHashSet(notLocked));
	}

	@Override
	public void purge() {
		deleted.clear();
	}

	@Override
	public void completed(CleanupRequest request, Iterable<Identifier> processed) {
        Iterables.removeAll(releasing, asList(toArray(processed, Identifier.class)));
    }
}
