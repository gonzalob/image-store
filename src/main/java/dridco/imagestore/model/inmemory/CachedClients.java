package dridco.imagestore.model.inmemory;

import com.google.common.base.Optional;
import dridco.imagestore.model.Client;
import dridco.imagestore.model.Clients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CachedClients implements Clients {

    private static final Logger LOGGER = LoggerFactory.getLogger(CachedClients.class);

    private Map<String, Optional<Client>> cache;
    private Collection<Client> all;
    private Clients proxee;

    public CachedClients(Clients proxee) {
        this.proxee = proxee;
        all = null;
        cache = Collections.synchronizedMap(new HashMap<String, Optional<Client>>());
    }

    @Override
    public Optional<Client> get(String clientKey) {
        Optional<Client> cached = cache.get(clientKey);
        if (cached == null) {
            LOGGER.info("Cache miss for client {}, fetching from underlying clients", clientKey);
            Optional<Client> actual = proxee.get(clientKey);
            cache.put(clientKey, actual);
            cached = actual;
        } else {
            LOGGER.debug("Cache hit for {}", clientKey);
        }
        return cached;
    }

    @Override
    public Collection<Client> getAll() {
        if (all == null) {
            LOGGER.info("Requesting and caching all clients from underlying clients");
            all = Collections.unmodifiableCollection(proxee.getAll());
        } else {
            LOGGER.debug("Retrieving cached clients");
        }
        return all;
    }

    public void refresh() {
        LOGGER.info("Emptying cached values for clients");
        all = null;
        cache.clear();
    }
}
