package dridco.imagestore.model.inmemory;

import com.google.common.base.Predicate;
import dridco.imagestore.model.Identifier;

import java.util.ArrayList;
import java.util.Collection;

public class NotLockedPredicate implements Predicate<Identifier> {

    static Predicate<Identifier> notLocked(Collection<Identifier> identifiers) {
        return new NotLockedPredicate(new ArrayList<>(identifiers));
    }

    private Collection<Identifier> identifiers;

    private NotLockedPredicate(Collection<Identifier> identifiers) {
        this.identifiers = identifiers;
    }

    @Override
    public boolean apply(Identifier input) {
        return !identifiers.contains(input);
    }
}
