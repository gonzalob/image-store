package dridco.imagestore.model.inmemory;

import com.google.common.base.Predicate;
import dridco.imagestore.model.Client;
import dridco.imagestore.model.Identifier;

public class BelongingToPredicate implements Predicate<Identifier> {

	private Client candidate;

	public static BelongingToPredicate belongingTo(Client candidate) {
		return new BelongingToPredicate(candidate);
	}

	public BelongingToPredicate(Client candidate) {
		this.candidate = candidate;
	}

	public boolean apply(Identifier identifier) {
		return identifier.isOwnedBy(candidate);
	}
}
