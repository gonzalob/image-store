package dridco.imagestore.model;

import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static java.util.Collections.unmodifiableMap;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.MediaType.*;

public class MagicNumberCandidate implements Candidate {

    public static final Map<MediaType, byte[]> SUPPORTED_TYPES = unmodifiableMap(supportedTypes());

    private final MediaType type;
    private final byte[] content;

    private static Map<MediaType, byte[]> supportedTypes() {
        Map<MediaType, byte[]> supportedTypes = newHashMap();
        supportedTypes.put(IMAGE_JPEG, new byte[]{-1, -40, -1});
        supportedTypes.put(IMAGE_GIF, new byte[]{71, 73, 70, 56});
        supportedTypes.put(IMAGE_PNG, new byte[]{-119, 80, 78, 71});

        getLogger(MagicNumberCandidate.class).info("Supported content types: " + supportedTypes.keySet());
        return supportedTypes;
    }

    public MagicNumberCandidate(MediaType type, byte[] bytes) throws UnsupportedContentType {
        validateContent(type, bytes);

        this.type = type;
        this.content = bytes;
    }

    protected void validateContent(MediaType type, byte[] content) throws UnsupportedContentType {
        byte[] expected = SUPPORTED_TYPES.get(type);

        if (expected == null
                || content.length < expected.length
                || !headersMatch(expected, content)) {
            throw new UnsupportedContentType(type, Arrays.copyOfRange(content, 0, content.length));
        }
    }

    private boolean headersMatch(byte[] expected, byte[] content) {
        for (int i = 0; i < expected.length; i++) {
            if (expected[i] != content[i]) {
                return false;
            }
        }
        return true;
    }

    @Override
    public MediaType getType() {
        return type;
    }

    @Override
    public byte[] getContent() {
        return content;
    }
}
