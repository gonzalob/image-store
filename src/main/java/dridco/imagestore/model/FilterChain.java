package dridco.imagestore.model;

import dridco.imagestore.model.filters.ImageFilterException;

import java.io.IOException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.unmodifiableList;

public class FilterChain implements ImageFilter {

	private List<ImageFilter> filters;

	public FilterChain(List<ImageFilter> filters) {
		this.filters = newArrayList(filters);
	}

	@Override
	public byte[] filter(byte[] content) throws IOException, ImageFilterException {
		byte[] filtered = content;
		for (ImageFilter filter : filters) {
			filtered = filter.filter(content);
		}
		return filtered;
	}

	public List<ImageFilter> getFilters() {
		return unmodifiableList(filters);
	}
}
