package dridco.imagestore.model;

import java.net.MalformedURLException;
import java.net.URL;

public interface URLFactory {

	URL urlFor(Object identifier) throws MalformedURLException;
}
