package dridco.imagestore.model;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class Image {

	private final Identifier identifier;

	public abstract byte[] getContent();

	public Image(Identifier identifier) {
		this.identifier = checkNotNull(identifier);
	}

	public String getEntityTag() {
		return "\"" + identifier.getHash() + "\"";
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public boolean entityTagMatches(String eTag) {
		return getEntityTag().equals(eTag);
	}

	@Override
	public String toString() {
		return identifier.getHash();
	}
}
