package dridco.imagestore.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;
import static dridco.imagestore.model.Dates.subtractMinutes;

public class Client {

	private static final int HASHCODE_INITIAL_VALUE = 28319;
	private static final int HASHCODE_MULTIPLIER = 23473;

	private String key;
	private URL callback;
	private Long initialValidity;
	private Long validityInterval;
	private URLFactory urlFactory;

	public Client(String key, Long initialValidity, Long validityInterval, URL callback, URLFactory urlFactory) {
		this.key = checkNotNull(key);
		this.initialValidity = checkNotNull(initialValidity);
		this.validityInterval = checkNotNull(validityInterval);
		this.callback = checkNotNull(callback);
		this.urlFactory = checkNotNull(urlFactory);
	}

	public Date gracePeriod(Date now) {
		return subtractMinutes(now, initialValidity);
	}

	public Date validity(Date now) {
		return subtractMinutes(now, validityInterval);
	}

	public URL getCallbackUrl() {
		return callback;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(HASHCODE_INITIAL_VALUE, HASHCODE_MULTIPLIER).append(key).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		return new EqualsBuilder().append(key, other.key).isEquals();
	}

	@Override
	public String toString() {
		return key;
	}

	public URL publicUrl(String identifier) throws MalformedURLException {
		return urlFactory.urlFor(identifier);
	}
}
