package dridco.imagestore.model;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

public class FindIdentifierWithHash implements Predicate<Identifier> {

	private String hash;

	public static FindIdentifierWithHash linkingTo(String hash) {
		return new FindIdentifierWithHash(hash);
	}

	public FindIdentifierWithHash(String hash) {
		this.hash = Preconditions.checkNotNull(hash);
	}

	@Override
	public boolean apply(Identifier input) {
		return hash.equals(input.getHash());
	}

}
