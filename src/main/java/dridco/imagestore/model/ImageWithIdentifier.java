package dridco.imagestore.model;

import com.google.common.base.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;

public class ImageWithIdentifier implements Predicate<Image> {

	public static ImageWithIdentifier identifiedBy(Identifier identifier) {
		return new ImageWithIdentifier(identifier);
	}

	private Identifier identifier;

	public ImageWithIdentifier(Identifier identifier) {
		this.identifier = checkNotNull(identifier);
	}

	public boolean apply(Image image) {
		return identifier.equals(image.getIdentifier());
	}
}
