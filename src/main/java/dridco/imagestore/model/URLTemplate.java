package dridco.imagestore.model;

import org.springframework.web.util.UriTemplate;

import java.net.MalformedURLException;
import java.net.URL;

public class URLTemplate implements URLFactory {

	private UriTemplate template;

	public URLTemplate(String template) {
		this.template = new UriTemplate(template);
	}

	@Override
	public URL urlFor(Object identifier) throws MalformedURLException {
		return template.expand(identifier).toURL();
	}
}
