package dridco.imagestore.model;

import static com.google.common.base.Preconditions.checkNotNull;

public class RawImage extends Image {

    private final byte[] content;

    public RawImage(Identifier identifier, byte[] content) {
        super(identifier);
        this.content = checkNotNull(content);
    }

    @Override
    public byte[] getContent() {
        return content;
    }
}
