package dridco.imagestore.model;

import com.google.api.client.util.Base64;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import static dridco.imagestore.model.MagicNumberCandidate.SUPPORTED_TYPES;
import java.util.Arrays;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;

import java.util.HashMap;
import java.util.Map;

/**
 * This write-only replica updater will send every referenced image to a replicated image store.
 */
public class UpdateReplica extends StorageEventListenerTemplate {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateReplica.class);

    private final CloseableHttpClient http;
    private final HttpHost replica;
    private final Map<String, String> replicaCredentials;

    public UpdateReplica(CloseableHttpClient client, HttpHost host, Map<String, String> plainTextCredentials) {
        http = client;
        replica = host;
        replicaCredentials = new HashMap<>(plainTextCredentials);

        LOGGER.info("Replicating images to {}", replica);
    }

    @Async
    @Override
    public void referenced(Image image) {
        try (CloseableHttpResponse result = http.execute(replica, put(image))) {
            LOGGER.info("Replicated {}. Response is {}", image, result.getStatusLine());
        } catch (Exception e) {
            LOGGER.error("Failed to replicate {}", image, e);
        }
    }

    private HttpPut put(Image image) {
        final int MAX_HEADER_LENGTH = 4;

        HttpPut put = new HttpPut("/" + image.getIdentifier().toString());
        put.setHeader(HttpHeaders.AUTHORIZATION, credentialsFor(image.getIdentifier().getOwner()));
        put.setHeader(HttpHeaders.CONTENT_TYPE, mediaTypeFrom(Arrays.copyOfRange(image.getContent(), 0, MAX_HEADER_LENGTH)).toString());
        put.setEntity(new ByteArrayEntity(image.getContent()));
        return put;
    }
    private String credentialsFor(final Client owner) {
        return Optional.fromNullable(replicaCredentials.get(owner.toString())).transform(new Function<String, String>() {
            @Override
            public String apply(String input) {
                return "Basic " + Base64.encodeBase64String((owner.toString() + ":" + input).getBytes());
            }
        }).orNull();
    }

    @Override
    public String toString() {
        return "Replicating to " + replica.toString();
    }

    private MediaType mediaTypeFrom(byte[] firstBytes) {
        final int MIN_HEADER_LENGTH = 2;
        final Map<MediaType, byte[]> knownTypes = SUPPORTED_TYPES;
        for(Map.Entry<MediaType, byte[]> knownType : knownTypes.entrySet()) {
            if(Arrays.equals(firstBytes, knownType.getValue()))
                return knownType.getKey();
        }
        // couldn't find a type for this header, can we try with a shorter one?
        if(firstBytes.length > MIN_HEADER_LENGTH)
            return mediaTypeFrom(Arrays.copyOfRange(firstBytes, 0, firstBytes.length - 1));
        else
            throw new IllegalArgumentException("Could not find a type for content");
    }
}
