package dridco.imagestore.model;

import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

public class Get implements Command<Image> {

	private String id;
	private StorageEventListener listener;

	public Get(String id, StorageEventListener listener) {
		this.id = checkNotNull(id);
		this.listener = checkNotNull(listener);
	}

	@Override
	public Image execute(Identifiers identifiers, Images images, Date when) throws IdentifiersException {
		Identifier identifier = identifiers.get(id);
		Image loaded = images.get(identifier);
		listener.loaded(loaded);
		return loaded;
	}
}
