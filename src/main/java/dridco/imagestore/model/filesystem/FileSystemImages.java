package dridco.imagestore.model.filesystem;

import dridco.imagestore.model.*;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Iterator;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Throwables.propagate;
import static java.nio.file.Files.*;

public class FileSystemImages implements Images {

	private Path root;

	public FileSystemImages(Path root) {
		this.root = checkNotNull(root);
	}

	@Override
	public Image get(Identifier identifier) {
		try {
			Path path = root.resolve(identifier.getHash());
			return new RawImage(identifier, Files.readAllBytes(path));
		} catch (NoSuchFileException e) {
			return new NoContent(identifier, e);
		} catch (IOException e) {
			throw propagate(e);
		}
	}

	@Override
	public boolean store(RawImage created) {
		try {
			Path path = root.resolve(created.getIdentifier().getHash());
			if(!path.toFile().exists()) {
				write(path, created.getContent());
				return true;
			}
			return false;
		} catch (IOException e) {
			throw propagate(e);
		}
	}

	@Override
	public void remove(String hash) {
		try {
			Path path = root.resolve(hash);
			delete(path);
		} catch (IOException e) {
			throw propagate(e);
		}
	}

	@Override
	public Integer count() {
		int count;
		try (DirectoryStream<Path> stream = newDirectoryStream(root)) {
			Iterator<Path> iterator = stream.iterator();
			for (count = 0; iterator.hasNext(); count++) {
				iterator.next();
			}
		} catch (IOException e) {
			throw propagate(e);
		}
		return count;
	}
}
