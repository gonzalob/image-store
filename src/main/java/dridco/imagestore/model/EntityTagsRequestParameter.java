package dridco.imagestore.model;

import java.util.List;

import static com.google.common.collect.Iterables.tryFind;
import static dridco.imagestore.model.MatchingEntityTag.matching;

public class EntityTagsRequestParameter implements Hash {

	private List<String> eTags;

	public EntityTagsRequestParameter(List<String> eTags) {
		this.eTags = eTags;
	}

	@Override
	public boolean matches(Image image) {
		return tryFind(eTags, matching(image)).isPresent();
	}
}
