package dridco.imagestore.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class FilterRegistryBuilder {

	private final Map<String, ImageFilterFactory> filters;
	private final String splitter;

	public FilterRegistryBuilder() {
		this(new HashMap<String, ImageFilterFactory>(), EMPTY);
	}

	private FilterRegistryBuilder(Map<String, ImageFilterFactory> filters, String splitter, String id, ImageFilterFactory filter) {
		this(add(filters, id, filter), splitter);
	}

	private FilterRegistryBuilder(Map<String, ImageFilterFactory> filters, String splitter) {
		this.filters = Collections.unmodifiableMap(filters);
		this.splitter = splitter;
	}

	private static Map<String, ImageFilterFactory> add(Map<String, ImageFilterFactory> current, String id, ImageFilterFactory filter) {
		Map<String, ImageFilterFactory> all = new HashMap<>(current);
		all.put(id, filter);
		return all;
	}

	public FilterRegistryBuilder withNameConfigSplitter(String splitter) {
		return new FilterRegistryBuilder(filters, splitter);
	}

	public FilterRegistryBuilder addFilter(String id, ImageFilterFactory factory) {
		return new FilterRegistryBuilder(filters, splitter, id, factory);
	}

	public FilterRegistry build() {
		return splitter.isEmpty() ? new FilterRegistry(filters) : new FilterRegistry(filters, splitter);
	}
}
