package dridco.imagestore.model;

public class StorageEventListenerTemplate implements StorageEventListener {

	@Override
	public void stored(Image image) {
	}

	@Override
	public void released(Identifier identifier) {
	}

	@Override
	public void removed(Image image) {
	}

	@Override
	public void loaded(Image image) {
	}

	@Override
	public void referenced(Image image) {
	}
}
