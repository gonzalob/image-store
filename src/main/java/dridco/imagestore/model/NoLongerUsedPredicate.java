package dridco.imagestore.model;

import com.google.common.base.Predicate;

import java.net.URL;

public class NoLongerUsedPredicate implements Predicate<Identifier> {

	public static NoLongerUsedPredicate noLongerUsed(IdentifierValidator validator, URL callback) {
		return new NoLongerUsedPredicate(validator, callback);
	}

	private final URL callback;
	private final IdentifierValidator validator;

	public NoLongerUsedPredicate(IdentifierValidator validator, URL callback) {
		this.validator = validator;
		this.callback = callback;
	}

	@Override
	public boolean apply(Identifier input) {
		return !validator.stillInUse(callback, input);
	}
}
