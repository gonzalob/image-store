package dridco.imagestore.model;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Lists.newArrayList;
import static dridco.imagestore.model.NoLongerUsedPredicate.noLongerUsed;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class GarbageCollector {

	private static final Logger LOG = LoggerFactory.getLogger(GarbageCollector.class);
	private static final AnyHash ANY_HASH = new AnyHash();
	
	private Clients clients;
	private Images images;
	private Identifiers identifiers;
	private IdentifierValidator validator;
	private StorageEventListener listener;

	public GarbageCollector(Clients clients, Identifiers identifiers, Images images, IdentifierValidator validator, StorageEventListener listener) {
		this.clients = checkNotNull(clients);
		this.identifiers = checkNotNull(identifiers);
		this.images = checkNotNull(images);
		this.validator = checkNotNull(validator);
		this.listener = checkNotNull(listener);
	}

	GarbageCollector(Clients clients, Identifiers identifiers, Images images, IdentifierValidator validator) {
		this(clients, identifiers, images, validator, new StorageEventListenerTemplate());
    }

	public void execute(Date when) throws IdentifiersException {
		LOG.info("Starting garbage collection");
		Stopwatch watch = Stopwatch.createStarted();
		for (Client client : clients.getAll()) {
			LOG.debug("Starting garbage collection for client {}", client);
			execute(new CleanupRequest(client, when), when);
		}
		watch.stop();
		LOG.info("Finished garbage collection run. Took {} millis", watch.elapsed(MILLISECONDS));
	}

	private void execute(CleanupRequest request, Date now) throws IdentifiersException {
		Iterable<Identifier> candidates   = identifiers.candidatesToRelease(request);
		    List<Identifier> toRemove     = newArrayList(filter(candidates, noLongerUsed(validator, request.getCallbackUrl())));
		Iterable<Identifier> toRevalidate = newArrayList(filter(candidates, not(in(toRemove))));

		remove(toRemove, now);
		revalidate(toRevalidate, now);

		identifiers.completed(request, candidates);
	}

	private void remove(Iterable<Identifier> toRemove, Date when) throws IdentifiersException {
		for (Identifier it : toRemove) {
			try {
				LOG.trace("Releasing identifier {}", it);
				new Release(it.toString(), ANY_HASH, listener).execute(identifiers, images, when);
			} catch (HashMismatch e) {
				throw new AssertionError("Using AnyHash, it should never be the case of a hash mismatch");
			}
		}
	}

	private void revalidate(Iterable<Identifier> toRevalidate, Date when) {
		for (Identifier it : toRevalidate) {
			LOG.trace("Revalidating identifier {}", it);
			it.validatedOn(when);
		}
	}
}
