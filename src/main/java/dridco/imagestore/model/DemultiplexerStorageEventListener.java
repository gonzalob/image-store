package dridco.imagestore.model;

public class DemultiplexerStorageEventListener implements StorageEventListener {

	private Iterable<StorageEventListener> listeners;

	public DemultiplexerStorageEventListener(Iterable<StorageEventListener> listeners) {
		this.listeners = listeners;
	}

	@Override
	public void loaded(Image image) {
		for (StorageEventListener each : listeners) {
			each.loaded(image);
		}
	}

	@Override
	public void referenced(Image image) {
		for (StorageEventListener each : listeners) {
			each.referenced(image);
		}
	}

	@Override
	public void stored(Image image) {
		for (StorageEventListener each : listeners) {
			each.stored(image);
		}
	}

	@Override
	public void released(Identifier identifier) {
		for (StorageEventListener each : listeners) {
			each.released(identifier);
		}
	}

	@Override
	public void removed(Image image) {
		for (StorageEventListener each : listeners) {
			each.removed(image);
		}
	}
}
