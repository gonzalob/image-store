package dridco.imagestore.model;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import static org.apache.http.HttpStatus.SC_GONE;

public class HttpClientIdentifierValidator implements IdentifierValidator {

	private static final String RESOURCE_REQUEST_PARAMETER = "resource";
	private static final String TIMESTAMP_REQUEST_PARAMETER = "timestamp";
	private static final Logger LOG = LoggerFactory.getLogger(HttpClientIdentifierValidator.class);
	private CloseableHttpClient http;
	private Clock clock;

	public HttpClientIdentifierValidator(CloseableHttpClient http, Clock clock) {
		this.http = http;
		this.clock = clock;
	}

	@Override
	public boolean stillInUse(URL callbackUrl, Identifier identifier) {
		try {
			URI requestUri = requestUri(callbackUrl, identifier);
			int statusCode = queryClient(identifier, requestUri);
			LOG.debug("Client response for {} was {}", identifier.getPublicUrl().toString(), statusCode);
			return statusCode != SC_GONE;
		} catch (URISyntaxException | IOException e) {
			LOG.warn("Failed to validate identifier {}. Assuming it is still used", identifier);
			return true;
		}
	}

	private int queryClient(Identifier identifier, URI requestUri) throws IOException {
		HttpGet get = new HttpGet(requestUri);
		try(CloseableHttpResponse response = http.execute(get)) {
			LOG.info("Requesting validation for {} to {}", identifier.getPublicUrl().toString(), requestUri);
			return response.getStatusLine().getStatusCode();
		}
	}

	private URI requestUri(URL callbackUrl, Identifier identifier) throws URISyntaxException {
		return new URIBuilder(callbackUrl.toURI())
			.addParameter(RESOURCE_REQUEST_PARAMETER, identifier.getPublicUrl().toString())
			.addParameter(TIMESTAMP_REQUEST_PARAMETER, String.valueOf(clock.now().getTime())).build();
	}
}
