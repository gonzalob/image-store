package dridco.imagestore.model.jdbc;

import dridco.imagestore.model.AsRequested;
import dridco.imagestore.model.Client;
import dridco.imagestore.model.URLFactory;
import dridco.imagestore.model.URLTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.google.common.base.Throwables.propagate;

public class ClientRowMapper implements RowMapper<Client> {

	@Override
	public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
		try {
			return new Client(
					rs.getString("username"),
					rs.getLong("grace_period"),
					rs.getLong("pruning_interval"),
					new URL(rs.getString("callback_url")),
					urlFactory(rs.getString("url_override")));
		} catch (MalformedURLException e) {
			throw propagate(e);
		}
	}

	private URLFactory urlFactory(String pattern) {
		if(pattern == null) {
			return new AsRequested();
		} else {
			return new URLTemplate(pattern);
		}
	}
}
