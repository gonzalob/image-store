package dridco.imagestore.model.jdbc;

import dridco.imagestore.model.*;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;

@Transactional
public class JdbcIdentifiers implements Identifiers, IdentifierEventListener {

    private static final Logger LOG = LoggerFactory.getLogger(JdbcIdentifiers.class);
	private static final String SELECT_WITHOUT_FILTERS = "SELECT identifier, hash, owner, created, last_checked, public_uri FROM identifiers";
	private static final String LIQUIBASE_MIGRATIONS = "jdbc-identifiers.yaml";
	private static final String LIQUIBASE_CONTEXTS = null;

    private Object candidateLock;
    private JdbcTemplate template;
	private IdentifierRowMapper rowMapper;

	public JdbcIdentifiers(DataSource database, Clients clients) throws SQLException, LiquibaseException {
		checkNotNull(database);
		runMigrations(database);
		template = new JdbcTemplate(database);
		rowMapper = new IdentifierRowMapper(this, clients);
        candidateLock = UUID.randomUUID();
        LOG.info("Using {} to lock release candidates", candidateLock);
	}

	private void runMigrations(DataSource dataSource) throws SQLException, LiquibaseException {
		try (Connection connection = dataSource.getConnection()) {
			Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
			Liquibase liquibase = new Liquibase(LIQUIBASE_MIGRATIONS, new ClassLoaderResourceAccessor(), database);
			liquibase.update(LIQUIBASE_CONTEXTS);
		}
	}

	@Transactional(readOnly = true)
	@Override
	public boolean contains(String id) {
		return template.queryForObject(
				"SELECT COUNT(1) FROM identifiers WHERE identifier = ?", Integer.class, id) > 0;
	}

	@Transactional(readOnly = true)
	@Override
	public Identifier get(String id) throws IdentifiersException {
        checkNotDeleted(id);
        checkExisting(id);
        return template.queryForObject(
				SELECT_WITHOUT_FILTERS + " WHERE identifier = ?", rowMapper, id);
	}

    private void checkExisting(String id) throws IdentifierDoesNotExist {
        if (!contains(id)) {
			throw new IdentifierDoesNotExist();
		}
    }

    private void checkNotDeleted(String id) throws IdentifierDeleted {
        if(template.queryForObject("SELECT COUNT(1) FROM deleted_identifiers WHERE identifier = ?", Boolean.class, id)) {
            throw new IdentifierDeleted();
        }
    }

	@Override
	public void store(Identifier identifier) throws IdentifiersException {
        String id = identifier.toString();
        checkNotDeleted(id);
        checkNonExisting(id);
		template.update(
				"INSERT INTO identifiers(identifier, hash, owner, created, last_checked, public_uri) VALUES(?, ?, ?, ?, ?, ?)",
                id, identifier.getHash(), identifier.getOwner().toString(), identifier.getCreated(), identifier.getLastChecked(), identifier.getPublicUrl().toString());
	}

    private void checkNonExisting(String id) throws IdentifierExists {
        if(contains(id)) {
			throw new IdentifierExists();
		}
    }

	@Override
	public Reachability remove(Identifier identifier) throws IdentifierDoesNotExist {
		String id = identifier.toString();
		if (template.update("DELETE FROM identifiers WHERE identifier = ?", id) != 1) {
			throw new IdentifierDoesNotExist();
		}
		template.update("INSERT INTO deleted_identifiers(identifier) VALUES(?)", id);

		if (template.queryForObject(
				"SELECT COUNT(1) FROM identifiers WHERE hash = ?", Integer.class, identifier.getHash()) > 0) {
			return new StillLinked();
		} else {
			return new Orphaned(identifier);
		}
	}

	@Override
	public Collection<Identifier> candidatesToRelease(CleanupRequest collector) {
        String owner = collector.getOwner().toString();
        String lock = candidateLock.toString();
        if (template.update(
                "UPDATE garbage_collector_locks SET handler = ? WHERE owner = ? AND handler IS NULL",
                lock, owner
        ) == 1) {
            String findCandidates = SELECT_WITHOUT_FILTERS + " WHERE owner = ? AND created < ? AND last_checked < ?";
            Date dueGracePeriod = collector.getDueGracePeriod();
            Date dueValidity = collector.getDueValidity();
            return template.query(findCandidates, rowMapper, owner, dueGracePeriod, dueValidity);
        } else {
            String currentLocker = template.queryForObject(
                    "SELECT handler FROM garbage_collector_locks WHERE owner = ?", String.class, owner
            );
            LOG.debug("Identifiers owned by {} are currently locked by {}", owner, currentLocker);
            return Collections.emptyList();
        }
    }

	@Override
	public void purge() {
		template.update("DELETE FROM deleted_identifiers");
	}

    @Override
    public void completed(CleanupRequest request, Iterable<Identifier> processed) {
        template.update(
                "UPDATE garbage_collector_locks SET handler = NULL WHERE handler = ? AND owner = ?",
                candidateLock.toString(), request.getOwner().toString()
        );
    }

    @Override
	public void validated(Identifier identifier, Date when) {
		template.update("UPDATE identifiers SET last_checked = ? WHERE identifier = ?", when, identifier.toString());
	}

	@PreDestroy
	public void releasePendingLocks() {
        String lock = candidateLock.toString();
        LOG.info("Releasing locked candidates on shutdown. Key was {}", lock);
        template.update("UPDATE garbage_collector_locks SET handler = NULL WHERE handler = ?", lock);
	}

    void createLockFor(Client owner) {
	    LOG.info("Prepared lock entry for owner {}", owner);
        template.update("INSERT INTO garbage_collector_locks (owner, handler) VALUES (?, null)", owner.toString());
    }
}
