package dridco.imagestore.model.jdbc;

import com.google.common.base.Optional;
import dridco.imagestore.model.Client;
import dridco.imagestore.model.Clients;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.unmodifiableCollection;

public class JdbcClients implements Clients {

	private static final String UNFILTERED_QUERY = "SELECT username, grace_period, pruning_interval, callback_url, url_override FROM users";
	private JdbcTemplate template;
	private ClientRowMapper rowMapper;

	public JdbcClients(JdbcTemplate template) {
		this.template = checkNotNull(template);
		rowMapper = new ClientRowMapper();
	}

	@Transactional(readOnly = true)
	public Optional<Client> get(String id) {
		List<Client> clients = template.query(
				UNFILTERED_QUERY + " WHERE username = ?",
				rowMapper, id);
		if (clients.isEmpty()) {
			return Optional.absent();
		} else {
			assert clients.size() == 1 : "Querying by primary key can only return one result, or none. never more";
			return Optional.of(clients.get(0));
		}
	}

	@Transactional(readOnly = true)
	@Override
	public Collection<Client> getAll() {
		return unmodifiableCollection(template.query(UNFILTERED_QUERY, rowMapper));
	}
}
