package dridco.imagestore.model.jdbc;

import dridco.imagestore.model.Clients;
import dridco.imagestore.model.Identifier;
import org.springframework.jdbc.core.RowMapper;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.google.common.base.Throwables.propagate;

public class IdentifierRowMapper implements RowMapper<Identifier> {

	private Clients clients;
	private JdbcIdentifiers identifiers;

	public IdentifierRowMapper(JdbcIdentifiers identifiers, Clients clients) {
		this.identifiers = identifiers;
		this.clients = clients;
	}

	@Override
	public Identifier mapRow(ResultSet rs, int rowNum) throws SQLException {
		try {
			return new Identifier(
					clients.get(rs.getString("owner")).get(),
					rs.getString("identifier"),
					new URL(rs.getString("public_uri")),
					rs.getString("hash"),
					rs.getTimestamp("created"),
					rs.getTimestamp("last_checked"),
					identifiers);
		} catch (MalformedURLException e) {
			throw propagate(e);
		}
	}
}
