package dridco.imagestore.model.filters;

import static com.google.common.base.Preconditions.checkNotNull;

public class AsRequested implements Scaling {

	private WidthAndHeight widthAndHeight;

	public AsRequested(WidthAndHeight widthAndHeight) {
		this.widthAndHeight = checkNotNull(widthAndHeight);
	}

	@Override
	public Scale compute(Dimensions unused) {
		return new FixedScale(widthAndHeight);
	}

	@Override
	public WidthAndHeight getWidthAndHeight() {
		return widthAndHeight;
	}
}
