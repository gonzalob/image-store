package dridco.imagestore.model.filters;

import static com.google.common.base.Preconditions.checkNotNull;

public class FixedScale extends AbstractScale {

	private WidthAndHeight requested;

	public FixedScale(WidthAndHeight widthAndHeight) {
		this.requested = checkNotNull(widthAndHeight);
	}

	@Override protected WidthAndHeight getRequested() { return requested; }
	@Override protected Integer getLeft()             { return 0;         }
	@Override protected Integer getTop()              { return 0;         }
	@Override protected WidthAndHeight getResolved()  { return requested; }
}
