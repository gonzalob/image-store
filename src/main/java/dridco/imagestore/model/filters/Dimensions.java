package dridco.imagestore.model.filters;

import java.awt.image.RenderedImage;
import java.math.BigDecimal;

import static com.google.common.base.Preconditions.checkNotNull;

public class Dimensions {

    private RenderedImage image;

    public Dimensions(RenderedImage image) {
        this.image = checkNotNull(image);
    }

    public BigDecimal getWidth() {
        return asBigDecimal(image.getWidth());
    }

    public BigDecimal getHeight() {
        return asBigDecimal(image.getHeight());
    }

    private BigDecimal asBigDecimal(Integer what) {
        return new BigDecimal(what);
    }
}
