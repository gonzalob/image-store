package dridco.imagestore.model.filters;

import dridco.imagestore.model.filters.WidthAndHeight.Metric;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_EVEN;

public enum IgnoredDimension {

	WIDTH {

		@Override
		public Integer getWidth(WidthAndHeight original, WidthAndHeight requested) {
			BigDecimal ratio = ratio(original.getWidth(), original.getHeight());
			return ratio.multiply(requested.getHeight().asBigDecimal()).intValue();
		}

		@Override
		public Integer getHeight(WidthAndHeight original, WidthAndHeight requested) {
			return requested.getHeight().asInteger();
		}
	},
	HEIGHT {

		@Override
		public Integer getWidth(WidthAndHeight original, WidthAndHeight requested) {
			return requested.getWidth().asInteger();
		}

		@Override
		public Integer getHeight(WidthAndHeight original, WidthAndHeight requested) {
			BigDecimal ratio = ratio(original.getHeight(), original.getWidth());
			return ratio.multiply(requested.getWidth().asBigDecimal()).intValue();
		}
	};

	private static BigDecimal ratio(Metric divisor, Metric dividend) {
		return divisor.asBigDecimal().divide(dividend.asBigDecimal(), SCALE, HALF_EVEN);
	}

	private static final Integer SCALE = 6;

	public abstract Integer getWidth(WidthAndHeight original, WidthAndHeight requested);

	public abstract Integer getHeight(WidthAndHeight original, WidthAndHeight requested);
}