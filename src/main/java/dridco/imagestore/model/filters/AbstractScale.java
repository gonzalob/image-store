package dridco.imagestore.model.filters;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import static java.awt.Image.SCALE_DEFAULT;

public abstract class AbstractScale implements Scale {

	private static final ImageObserver NO_OBSERVER = null;

	protected abstract WidthAndHeight getRequested();
	protected abstract WidthAndHeight getResolved();
	protected abstract Integer getTop();
	protected abstract Integer getLeft();

	@Override
	public final BufferedImage create(BufferedImage image) {
		WidthAndHeight requested = getRequested();
		Integer height = requested.getHeight().asInteger();
		Integer width  = requested.getWidth().asInteger();
		BufferedImage scaled = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = scaled.createGraphics();
		configure(graphics);
		graphics.fillRect(0, 0, width, height);
		graphics.drawImage(image.getScaledInstance(getResolved().getWidth().asInteger(), getResolved().getHeight().asInteger(), SCALE_DEFAULT), getLeft(), getTop(), NO_OBSERVER);
		return scaled;
	}

	protected void configure(Graphics2D graphics) {}
}
