package dridco.imagestore.model.filters;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

public class ScaleOverflowing extends AbstractScale {

	private Integer left;
	private Integer top;
	private Color background;
	private WidthAndHeight requested;
	private WidthAndHeight resolved;

	public ScaleOverflowing(WidthAndHeight requested, WidthAndHeight resolved, Integer left, Integer top, Color background) {
		this.requested  = checkNotNull(requested);
		this.resolved   = checkNotNull(resolved);
		this.left       = checkNotNull(left);
		this.top        = checkNotNull(top);
		this.background = checkNotNull(background);
	}

	@Override protected WidthAndHeight getRequested() { return requested; }
	@Override protected Integer        getLeft()      { return left;      }
	@Override protected Integer        getTop()       { return top;       }
	@Override protected WidthAndHeight getResolved()  { return resolved;  }

	@Override
	protected void configure(Graphics2D graphics) {
		graphics.setColor(background);
	}
}
