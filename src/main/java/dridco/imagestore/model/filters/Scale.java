package dridco.imagestore.model.filters;

import java.awt.image.BufferedImage;

public interface Scale {

	BufferedImage create(BufferedImage original);
}
