package dridco.imagestore.model.filters;

public interface Scaling {

	Scale compute(Dimensions original);
	WidthAndHeight getWidthAndHeight();
}
