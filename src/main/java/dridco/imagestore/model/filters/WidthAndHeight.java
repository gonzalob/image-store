package dridco.imagestore.model.filters;

import java.math.BigDecimal;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

public final class WidthAndHeight {

	private final Metric width;
	private final Metric height;

	public WidthAndHeight(Integer width, Integer height) {
		this(new Metric(width), new Metric(height));
	}

	public WidthAndHeight(BigDecimal width, BigDecimal height) {
		this(new Metric(width), new Metric(height));
	}

	private WidthAndHeight(Metric width, Metric height) {
		this.width = width;
		this.height = height;
	}

	public Metric getHeight() { return height; }
	public Metric getWidth()  { return width;  }

	public boolean widthMatches(Integer other) {
		return width.equals(new Metric(other));
	}

	public boolean heightMatches(Integer other) {
		return height.equals(new Metric(other));
	}

	@Override
	public String toString() {
		return format("%sx%s", width, height);
	}

	public static final class Metric {

		private final BigDecimal value;

		public Metric(BigDecimal value) {
			this.value = checkNotNull(value);
		}

		public Metric(Integer value) {
			this(new BigDecimal(value));
		}

		public BigDecimal asBigDecimal() {
			return value;
		}

		public Integer asInteger() {
			return value.intValue();
		}

		@Override
		public int hashCode() {
			return reflectionHashCode(this);
		}

		@Override
		public boolean equals(Object obj) {
			return reflectionEquals(this, obj);
		}

		@Override
		public String toString() {
			return value.toString();
		}
	}
}
