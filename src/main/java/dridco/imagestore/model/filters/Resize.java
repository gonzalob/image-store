package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterators.getOnlyElement;
import static javax.imageio.ImageIO.*;

public class Resize implements ImageFilter {

	private Scaling scaling;

	public Resize(Scaling scaling) {
		this.scaling = checkNotNull(scaling);
	}

	@Override
	public byte[] filter(byte[] content) throws IOException, DecoderNotFound {
		BufferedImage image = read(new ByteArrayInputStream(content));
		if(image == null) throw new DecoderNotFound();
		Scale scale = scaling.compute(new Dimensions(image));
		BufferedImage scaled = scale.create(image);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(scaled, format(content), out);
		return out.toByteArray();
	}

	private String format(byte[] image) throws IOException {
		Object stream = createImageInputStream(new ByteArrayInputStream(image));
		return getOnlyElement(getImageReaders(stream)).getFormatName();
	}
	
	public Scaling getScaling() {
		return scaling;
	}
}
