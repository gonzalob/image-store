package dridco.imagestore.model.filters;

import java.awt.*;
import java.math.BigDecimal;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Math.min;
import static java.math.RoundingMode.HALF_EVEN;

public final class KeepAspectRatioOverflowing implements Scaling {

	private static final Integer SCALE = 6;

	private final Overflow vertical;
	private final Overflow horizontal;
	private final Color background;
	private final WidthAndHeight requested;

	public KeepAspectRatioOverflowing(WidthAndHeight requested, Overflow vertical, Overflow horizontal, Color background) {
		this.requested  = checkNotNull(requested);
		this.vertical   = checkNotNull(vertical);
		this.horizontal = checkNotNull(horizontal);
		this.background = checkNotNull(background);
	}

	@Override
	public WidthAndHeight getWidthAndHeight() {
		return requested;
	}

	@Override
	public final Scale compute(Dimensions original) {
		WidthAndHeight requested = getWidthAndHeight();
		// borrowed from http://stackoverflow.com/a/14731922
		BigDecimal width = original.getWidth();
		BigDecimal height = original.getHeight();
		BigDecimal ratio = asBigDecimal(min(
				divide(width, requested.getWidth().asBigDecimal()),
				divide(height, requested.getHeight().asBigDecimal())));

		return scaleFrom(width, height, ratio);
	}

	private double divide(BigDecimal actual, BigDecimal resized) {
		return resized.divide(actual, SCALE, HALF_EVEN).doubleValue();
	}

	private BigDecimal asBigDecimal(Double value) {
		return new BigDecimal(value);
	}

	private ScaleOverflowing scaleFrom(BigDecimal width, BigDecimal height, BigDecimal ratio) {
		BigDecimal newWidth = width.multiply(ratio);
		BigDecimal newHeight = height.multiply(ratio);
		return new ScaleOverflowing(
				requested,
				new WidthAndHeight(newWidth, newHeight),
				horizontal.compute(requested.getWidth().asBigDecimal(), newWidth),
				vertical.compute(requested.getHeight().asBigDecimal(), newHeight),
				background);
	}
}
