package dridco.imagestore.model.filters;

import java.math.BigDecimal;

public interface Overflow {

	Integer compute(BigDecimal requested, BigDecimal actual);
}
