package dridco.imagestore.model.filters;

import dridco.imagestore.model.Identifier;

public class DecoderNotFound extends ImageFilterException {

    @Override
    protected String getMessageWithIdentifier(Identifier identifier) {
        return String.format("No suitable decoder to read image under identifier %s", identifier);
    }
}
