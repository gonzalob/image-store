package dridco.imagestore.model.filters;

import com.google.common.base.Preconditions;
import dridco.imagestore.model.ImageFilterFactory;

import java.awt.*;

/**
 * This factory expects to receive at least a string specifying the desired 
 * width and height, in the form <b>[width]x[height]</b>.<br>
 * 
 * Additionally, the settings string can also contain information for handling 
 * the overflowing when respecting the aspect ratio. Note that if this information 
 * is not defined, the ratio won't be considered and the image may be altered.<br>
 * 
 * The string must be in one of two forms:
 * 
 * <h1><b>.[vhrrggbb]</b>, meaning:<br>
 * <ul>
 * <li>.  : a literal dot
 * <li>v  : the position to place the image if a vertical overflow occurs
 * <li>h  : the position to place the image if an horizontal overflow occurs
 * <li>rr : hex-coded red
 * <li>gg : hex-coded green
 * <li>bb : hex-coded blue
 * </ul>
 * v and h will be used to place the image. Supported values here are <b>s</b>, <b>c</b> 
 * and <b>e</b> where:<br>
 * <ul>
 * <li>- s means start (which'll fill the bottom or rightmost part of the picture 
 * with the background color)
 * <li>- c means center (the picture will be surrounded by the background color)
 * <li>- e means end (the background color will appear before the picture)
 * </ul>
 * rrggbb compose the background color. 000000 means black, ffffff means white.<br>
 * For more information on this, see <a href="http://bit.ly/1DrUvOM">Wikipedia</a>
 * 
 * <h1><b>.i[hw]</b>
 * This setup specifies the resizer must ignore one of the dimensions (scaling without 
 * adding any overflow, keeping the original ratio)
 * 
 * <ul>
 * <li>h is for height
 * <li>w is for width
 * </ul>
 * 
 * If setting the <b>i</b> flag, a dimension (no more, no less) must follow.
 * Defining a value for the dimension being ignored is required (cannot be empty) but will 
 * not be used.
 * 
 * <h1>Examples</h1>
 * <ul>
 * <li>300x200
 * <li>400x400.sc000000
 * <li>200x1500.ih
 * </ul>
 */
public class ResizeFactory implements ImageFilterFactory {

	@Override
	public Resize create(String settings) throws ImageFilterFactoryException {
		String[] sizeAndScaling = settings.split("\\.");
		try {
			WidthAndHeight dimensions = parseDimensions(sizeAndScaling[0]);
			Scaling scaling = sizeAndScaling.length == 1 ? asIs(dimensions) : keepAspectRatio(dimensions, sizeAndScaling[1]);
			return new Resize(scaling);
		} catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
			throw new ImageFilterFactoryException(e);
		}
	}

	private Scaling asIs(WidthAndHeight dimensions) {
		return new AsRequested(dimensions);
	}

	private Scaling keepAspectRatio(WidthAndHeight dimensions, String settings) throws ImageFilterFactoryException {
		return 'i' == settings.charAt(0) ? noOverflow(dimensions, settings) : withOverflow(dimensions, settings);
	}

	private Scaling noOverflow(WidthAndHeight dimensions, String settings) throws ImageFilterFactoryException {
		IgnoredDimension ignored = parseIgnoredDimension(settings);
		return new KeepAspectRatioComputingIgnoredDimension(dimensions, ignored);
	}

	private IgnoredDimension parseIgnoredDimension(String settings) throws ImageFilterFactoryException {
		if(settings.length() != 2) {
			throw new ImageFilterFactoryException("Only one dimension can be ignored");
		}
		switch(settings.charAt(1)) {
		case 'w': return IgnoredDimension.WIDTH;
		case 'h': return IgnoredDimension.HEIGHT;
		default : throw new ImageFilterFactoryException("Invalid dimension defined. Must be either 'h' or 'v'");
		}
	}

	private KeepAspectRatioOverflowing withOverflow(WidthAndHeight dimensions, String settings) throws ImageFilterFactoryException {
		Overflow vertical   = overflow(settings.charAt(0));
		Overflow horizontal = overflow(settings.charAt(1));
		Color    background = color(settings.substring(2));
		return new KeepAspectRatioOverflowing(dimensions, vertical, horizontal, background);
	}

	private Color color(String code) {
		Integer red   = parseColor(code);
		Integer green = parseColor(code.substring(2));
		Integer blue  = parseColor(code.substring(4));
		return new Color(red, green, blue);
	}

	private Integer parseColor(String value) {
		return Integer.decode("0x" + value.substring(0, 2));
	}

	private Overflow overflow(char key) throws ImageFilterFactoryException {
		switch(key) {
		case 'c': return new Middle();
		case 's': return new Start();
		case 'e': return new End();
		default : throw new ImageFilterFactoryException("Invalid overflow position selected");
		}
	}

	private WidthAndHeight parseDimensions(String settings) throws ImageFilterFactoryException {
		String[] values = settings.split("x");
		return new WidthAndHeight(Integer.valueOf(values[0]), Integer.valueOf(values[1]));
	}
}
