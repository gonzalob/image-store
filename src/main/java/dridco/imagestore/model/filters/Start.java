package dridco.imagestore.model.filters;

import java.math.BigDecimal;

public final class Start implements Overflow {

	@Override
	public Integer compute(BigDecimal requested, BigDecimal actual) {
		return 0;
	}
}
