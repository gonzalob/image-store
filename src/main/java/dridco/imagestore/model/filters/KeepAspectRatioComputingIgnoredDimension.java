package dridco.imagestore.model.filters;

public class KeepAspectRatioComputingIgnoredDimension implements Scaling {

	private IgnoredDimension ignored;
	private WidthAndHeight requested;

	public KeepAspectRatioComputingIgnoredDimension(WidthAndHeight requested, IgnoredDimension ignored) {
		this.requested = requested;
		this.ignored = ignored;
	}

	@Override
	public final Scale compute(Dimensions originalDimensions) {
		WidthAndHeight original = new WidthAndHeight(originalDimensions.getWidth(), originalDimensions.getHeight());
		return new FixedScale(new WidthAndHeight(
				ignored.getWidth(original, requested),
				ignored.getHeight(original, requested)));
	}

	@Override
	public WidthAndHeight getWidthAndHeight() {
		return requested;
	}
}
