package dridco.imagestore.model.filters;

import dridco.imagestore.model.Identifier;

public abstract class ImageFilterException extends Exception {

    private Identifier identifier;

    public final void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    @Override
    public String getMessage() {
        return (identifier != null) ? getMessageWithIdentifier(identifier) : super.getMessage();
    }

    protected abstract String getMessageWithIdentifier(Identifier identifier);
}
