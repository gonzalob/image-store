package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;

import java.io.IOException;

public class OriginalContentOnException implements ImageFilter {

    private ImageFilter filter;

    OriginalContentOnException(ImageFilter filter) {
        this.filter = filter;
    }

    @Override
    public byte[] filter(byte[] content) throws IOException, ImageFilterException {
        try {
            return filter.filter(content);
        } catch (ImageFilterException | IOException ignored) {
            return content;
        }
    }
}
