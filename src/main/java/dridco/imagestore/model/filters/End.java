package dridco.imagestore.model.filters;

import java.math.BigDecimal;

public final class End implements Overflow {

	@Override
	public Integer compute(BigDecimal requested, BigDecimal actual) {
		return requested.subtract(actual).intValue();
	}
}
