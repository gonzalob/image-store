package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;
import dridco.imagestore.model.ImageFilterFactory;

public class FailSafeFilterFactory implements ImageFilterFactory {

    private ImageFilterFactory filter;

    public FailSafeFilterFactory(ImageFilterFactory filter) {
        this.filter = filter;
    }

    @Override
    public ImageFilter create(String settings) throws ImageFilterFactoryException {
        return new OriginalContentOnException(filter.create(settings));
    }
}
