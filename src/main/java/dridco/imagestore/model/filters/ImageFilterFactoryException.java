package dridco.imagestore.model.filters;

public class ImageFilterFactoryException extends Exception {

	private static final long serialVersionUID = 8094212504468452510L;

	public ImageFilterFactoryException(Throwable cause) {
		super(cause);
	}

	public ImageFilterFactoryException(String message) {
		super(message);
	}
}
