package dridco.imagestore.model.filters;

import java.math.BigDecimal;

public final class Middle implements Overflow {

	private static final BigDecimal HALF = new BigDecimal(2);

	@Override
	public Integer compute(BigDecimal requested, BigDecimal actual) {
		return requested.subtract(actual).divide(HALF).intValue();
	}
}
