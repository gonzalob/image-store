package dridco.imagestore.model;

import org.springframework.http.MediaType;

import java.util.Arrays;

public class UnsupportedContentType extends ImageStoreException {

    public UnsupportedContentType(MediaType type, byte[] header) {
        super(String.format("Unsupported content for media type %s. Header is %s", type, Arrays.toString(header)));
    }
}
