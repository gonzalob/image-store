package dridco.imagestore.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.net.URL;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;
import static dridco.imagestore.model.Dates.copy;

public class Identifier {

	private final IdentifierEventListener listener;
	private final String id;
	private final String hash;
	private final Client owner;
	private final URL publicUrl;
	private Date validated;
	private Date created;

	public Identifier(Client owner, String id, URL publicUrl, String hash, Date created) {
		this(owner, id, publicUrl, hash, created, created);
	}

	public Identifier(Client owner, String id, URL publicUrl, String hash, Date created, Date validated) {
		this(owner, id, publicUrl, hash, created, validated, new IdentifierEventListenerTemplate());
	}

	public Identifier(Client owner, String id, URL publicUrl, String hash, Date created, Date validated, IdentifierEventListener listener) {
		this.publicUrl = checkNotNull(publicUrl);
		this.listener  = checkNotNull(listener);
		this.owner     = checkNotNull(owner);
		this.id        = checkNotNull(id);
		this.hash      = checkNotNull(hash);
		this.created   = copy(created);
		this.validated = copy(validated);
	}

	public boolean identifiedBy(String id) {
		return this.id.equals(id);
	}

	public String getHash() {
		return hash;
	}

	public Client getOwner() {
		return owner;
	}

	public Date getLastChecked() {
		return validated;
	}

	public Date getCreated() {
		return created;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identifier other = (Identifier) obj;
		return new EqualsBuilder().append(id, other.id).isEquals();
	}

	@Override
	public String toString() {
		return id;
	}

	public boolean isOwnedBy(Object requester) {
		return owner.equals(requester);
	}

	public void validatedOn(Date when) {
		validated = new Date(when.getTime());
		listener.validated(this, when);
	}

	public URL getPublicUrl() {
		return publicUrl;
	}
}
