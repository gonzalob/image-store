package dridco.imagestore.model;

public class ImageStoreException extends Exception {

	private static final long serialVersionUID = -287472726672492090L;

	public ImageStoreException() {}

	public ImageStoreException(Exception cause) {
		super(cause);
	}

	public ImageStoreException(String message) {
		super(message);
	}
}
