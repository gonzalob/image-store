package dridco.imagestore.model;

import java.util.Date;

public class SecuredCommand<T> implements Command<T> {

	private String id;
	private Object requester;
	private Command<T> proxee;

	public SecuredCommand(Object requester, String id, Command<T> proxee) {
		this.requester = requester;
		this.id = id;
		this.proxee = proxee;
	}

	@Override
	public T execute(Identifiers identifiers, Images images, Date when) throws ImageStoreException {
		Identifier identifier = identifiers.get(id);
		if(identifier.isOwnedBy(requester)) {
			return proxee.execute(identifiers, images, when);
		} else {
			throw new DoesNotBelongToRequester();
		}
	}
}
