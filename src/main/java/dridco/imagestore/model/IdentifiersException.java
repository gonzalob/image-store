package dridco.imagestore.model;

public class IdentifiersException extends ImageStoreException {

	private static final long serialVersionUID = 8146494003518890409L;

	public IdentifiersException() {}

	public IdentifiersException(Exception cause) {
		super(cause);
	}

}
