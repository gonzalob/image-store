package dridco.imagestore.model;

import java.util.Date;

public interface IdentifierEventListener {

	void validated(Identifier identifier, Date when);
}
