package dridco.imagestore.model;

import org.springframework.http.MediaType;

public interface Candidate {

    MediaType getType();

    byte[] getContent();
}
