package dridco.imagestore.model;

import org.apache.commons.codec.digest.DigestUtils;

import java.net.URL;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

public class Store implements Command<Image> {

    private Client owner;
    private String id;
    private byte[] content;
    private StorageEventListener listener;
    private URL publicUrl;

    public Store(Client owner, String id, URL publicUrl, Candidate candidate, StorageEventListener listener) {
        this.owner = checkNotNull(owner);
        this.id = checkNotNull(id);
        this.publicUrl = checkNotNull(publicUrl);
        this.content = candidate.getContent();
        this.listener = checkNotNull(listener);
    }

    @Override
    public RawImage execute(Identifiers identifiers, Images images, Date when)
            throws IdentifiersException {
        Identifier identifier = new Identifier(owner, id, publicUrl, hash(content), when);
        RawImage created = new RawImage(identifier, content);
        identifiers.store(identifier);
        listener.referenced(created);
        if (images.store(created)) {
            listener.stored(created);
        }
        return created;
    }

    private String hash(byte[] content) {
        return DigestUtils.shaHex(content);
    }
}
