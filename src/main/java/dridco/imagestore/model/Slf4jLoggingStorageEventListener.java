package dridco.imagestore.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slf4jLoggingStorageEventListener implements StorageEventListener {

	private static final Logger LOG = LoggerFactory.getLogger(Slf4jLoggingStorageEventListener.class);
	
	@Override
	public void loaded(Image image) {
		LOG.debug("Loaded image {} using identifier {}", image, image.getIdentifier());
	}

	@Override
	public void referenced(Image image) {
		LOG.info("Referenced image {} from identifier {}", image, image.getIdentifier());
	}

	@Override
	public void stored(Image image) {
		LOG.info("Stored new image {}", image);
	}

	@Override
	public void released(Identifier identifier) {
		LOG.info("Released reference {}", identifier);
	}

	@Override
	public void removed(Image image) {
		LOG.info("Image {} removed", image);
	}

	@Override
	public String toString() {
		return "Simple Logging Facade for Java";
	}
}
