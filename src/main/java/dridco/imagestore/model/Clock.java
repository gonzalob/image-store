package dridco.imagestore.model;

import java.util.Date;

public interface Clock {

	Date now();
}
