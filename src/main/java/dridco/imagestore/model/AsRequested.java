package dridco.imagestore.model;

import dridco.imagestore.controllers.ImagesController;

import java.net.MalformedURLException;
import java.net.URL;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class AsRequested implements URLFactory {

	@Override
	public URL urlFor(Object identifier) throws MalformedURLException {
		return linkTo(ImagesController.class).slash(identifier).toUri().toURL();
	}
}
