package dridco.imagestore.model;

import java.util.Date;

public interface Command<T> {

	T execute(Identifiers identifiers, Images images, Date when) throws ImageStoreException;
}
