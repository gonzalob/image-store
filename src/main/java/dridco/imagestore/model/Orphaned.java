package dridco.imagestore.model;

public class Orphaned implements Reachability {

	private Identifier identifier;

	public Orphaned(Identifier identifier) {
		this.identifier = identifier;
	}

	@Override
	public boolean remove(Images images) {
		images.remove(identifier.getHash());
		return true;
	}
}
