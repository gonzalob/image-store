package dridco.imagestore.model;

import java.net.URL;

public interface IdentifierValidator {

	boolean stillInUse(URL callbackUrl, Identifier identifier);
}
