package dridco.imagestore.model;

import java.net.URL;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

public class Alias implements Command<Image> {

	private Client owner;
	private String id;
	private String source;
	private URL publicUrl;
	private StorageEventListener listener;

	public Alias(Client owner, String id, String source, URL publicUrl, StorageEventListener listener) {
		this.owner = checkNotNull(owner);
		this.id = checkNotNull(id);
		this.source = checkNotNull(source);
		this.publicUrl = checkNotNull(publicUrl);
		this.listener = checkNotNull(listener);
	}

	@Override
	public Image execute(Identifiers identifiers, Images images, Date when) throws ImageStoreException {
		Identifier identifier = new Identifier(owner, id, publicUrl, source, when);
		identifiers.store(identifier);
		Image referenced = images.get(identifier);
		listener.referenced(referenced);
		return referenced;
	}

}
