package dridco.imagestore.model;

public class NoContent extends Image {

    private final Throwable cause;

    public NoContent(Identifier identifier, Throwable cause) {
        super(identifier);
        this.cause = cause;
    }

    @Override
    public byte[] getContent() {
        throw new NoContentException(cause);
    }

    static class NoContentException extends RuntimeException {

        public NoContentException(Throwable cause) {
            super(cause);
        }
    }
}
