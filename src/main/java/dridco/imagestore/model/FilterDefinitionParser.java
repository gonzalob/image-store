package dridco.imagestore.model;

import dridco.imagestore.model.filters.ImageFilterFactoryException;

import java.util.List;

public interface FilterDefinitionParser {

	ImageFilter parse(List<String> definitions) throws ImageFilterFactoryException;
}
