package dridco.imagestore.model;

public interface Hash {

	boolean matches(Image image);
}
