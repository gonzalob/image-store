package dridco.imagestore.model;

import dridco.imagestore.model.filters.ImageFilterFactoryException;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.EMPTY;

public class FilterRegistry implements FilterDefinitionParser {

	private static final String DEFAULT_NAME_CONFIG_SPLIT_REGEX = ":";

	private Map<String, ImageFilterFactory> filters;
	private String nameConfigSplitRegex;

	public FilterRegistry(Map<String, ImageFilterFactory> filters, String nameConfigSplitRegex) {
		this.filters = filters;
		this.nameConfigSplitRegex = nameConfigSplitRegex;
	}

	public FilterRegistry(Map<String, ImageFilterFactory> filters) {
		this(filters, DEFAULT_NAME_CONFIG_SPLIT_REGEX);
	}

	@Override
	public FilterChain parse(List<String> definitions) throws ImageFilterFactoryException {
		List<ImageFilter> selected = newArrayList();
		for (String definition : definitions) {
			String[] components = definition.split(nameConfigSplitRegex);
			String name = components[0];
			String config = components.length > 1 ? components[1] : EMPTY;
			ImageFilterFactory factory = filters.get(name);
			if (factory != null) {
				selected.add(factory.create(config));
			}
		}
		return new FilterChain(selected);
	}
}
