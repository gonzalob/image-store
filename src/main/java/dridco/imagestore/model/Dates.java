package dridco.imagestore.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.MINUTE;

public final class Dates {

	public static Date parse(String date) throws ParseException {
		return new SimpleDateFormat("yyy-MM-dd hh:mm:ss").parse(date);
	}

	public static Date subtractMinutes(Date date, Long minutes) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(MINUTE, -minutes.intValue());
		return calendar.getTime();
	}

	public static Date copy(Date source) {
		return new Date(source.getTime());
	}

	private Dates() {}
}
