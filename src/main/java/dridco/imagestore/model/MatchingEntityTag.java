package dridco.imagestore.model;

import com.google.common.base.Predicate;

public class MatchingEntityTag implements Predicate<String> {

	private Image image;

	public static MatchingEntityTag matching(Image image) {
		return new MatchingEntityTag(image);
	}
	
	public MatchingEntityTag(Image image) {
		this.image = image;
	}
	
	public boolean apply(String input) {
		return image.entityTagMatches(input);
	}
}
