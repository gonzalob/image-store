package dridco.imagestore.model;

public interface Identifiers {

	boolean contains(String id) throws IdentifiersException;

	Identifier get(String id) throws IdentifiersException;

	void store(Identifier identifier) throws IdentifiersException;

	Reachability remove(Identifier identifier) throws IdentifiersException;

	Iterable<Identifier> candidatesToRelease(CleanupRequest collector);

	void purge();

    void completed(CleanupRequest request, Iterable<Identifier> processed);
}
