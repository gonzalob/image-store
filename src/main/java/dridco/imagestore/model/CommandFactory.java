package dridco.imagestore.model;

import org.springframework.http.MediaType;

import java.net.URL;

import static com.google.common.base.Preconditions.checkNotNull;

public class CommandFactory {

	private StorageEventListener listener;

	public CommandFactory(StorageEventListener listener) {
		this.listener = checkNotNull(listener);
	}

	public Store store(Client owner, Candidate candidate, String identifier, URL publicUrl)
            throws UnsupportedContentType {
		return new Store(owner, identifier, publicUrl, candidate, listener);
	}

	public Get get(String id) {
		return new Get(id, listener);
	}

	public Command<Boolean> release(Object requester, String identifier, Hash hash) {
		return new SecuredCommand<>(requester, identifier, new Release(identifier, hash, listener));
	}

	public Command<Image> alias(Client owner, String id, String source, URL publicUrl) {
		return new Alias(owner, id, source, publicUrl, listener);
	}
}
