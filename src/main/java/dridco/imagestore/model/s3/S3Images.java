package dridco.imagestore.model.s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import dridco.imagestore.model.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;

import static com.amazonaws.regions.Region.getRegion;
import static com.amazonaws.util.IOUtils.toByteArray;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Throwables.propagate;
import static java.lang.String.format;
import static org.springframework.http.HttpStatus.NOT_FOUND;

public class S3Images implements Images {

	private AmazonS3 s3;
	private String bucket;

	public S3Images(String apiKey, String secret, Regions region, String bucketName) {
		AWSCredentials credentials = new BasicAWSCredentials(apiKey, secret);
		s3 = new AmazonS3Client(credentials);
		s3.setRegion(getRegion(region));
		this.bucket = checkNotNull(bucketName);
	}

	@Override
	public Image get(Identifier identifier)  {
		try(S3Object object = s3.getObject(new GetObjectRequest(bucket, identifier.getHash()))) {
			return new RawImage(identifier, toByteArray(object.getObjectContent()));
		} catch (AmazonS3Exception e) {
			return new NoContent(identifier, e);
		} catch(IOException e) {
 			throw new RuntimeException(format("Failed to load content for image with identifier %s", identifier), e);
		}
	}

	@Override
	public boolean store(RawImage created) {
		try {
			s3.getObjectMetadata(new GetObjectMetadataRequest(bucket, created.toString()));
			// the hash is in use
			return false;
		} catch(AmazonS3Exception e) {
			doStoreIfNotFound(created, e);
			return true;
		}
	}

	private void doStoreIfNotFound(Image created, AmazonS3Exception e) {
		if(NOT_FOUND.value() == e.getStatusCode()) {
			doStore(created);
		} else throw e;
	}

	private void doStore(Image created) {
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(created.getContent().length);
        try(ByteArrayInputStream input = new ByteArrayInputStream(created.getContent())) {
            s3.putObject(new PutObjectRequest(bucket, created.toString(), input, metadata));
        } catch (IOException e) {
            propagate(e);
        }
    }

	@Override
	public void remove(String hash) {
		s3.deleteObject(bucket, hash);
	}

	@Override
	public Integer count() {
		return all().size();
	}

	void clear() {
		for (S3ObjectSummary each : all()) {
			remove(each.getKey());
		}
	}

	private Collection<S3ObjectSummary> all() {
		ObjectListing all = s3.listObjects(new ListObjectsRequest().withBucketName(bucket));
		return all.getObjectSummaries();
	}
}