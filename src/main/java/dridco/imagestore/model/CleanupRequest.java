package dridco.imagestore.model;

import java.net.URL;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

public class CleanupRequest {

	private Client owner;
	private Date now;

	public CleanupRequest(Client owner, Date now) {
		this.owner = checkNotNull(owner);
		this.now = new Date(checkNotNull(now).getTime());
	}

	public Date getDueGracePeriod() {
		return owner.gracePeriod(now);
	}

	public Date getDueValidity() {
		return owner.validity(now);
	}

	public Client getOwner() {
		return owner;
	}

	@Override
	public int hashCode() {
		return reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return reflectionEquals(this, obj);
	}

	public URL getCallbackUrl() {
		return owner.getCallbackUrl();
	}
}
