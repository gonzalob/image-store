package dridco.imagestore.model;

public interface Images {

	Image get(Identifier identifier);

	boolean store(RawImage created);

	void remove(String hash);

	Integer count();
}
