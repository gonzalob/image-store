package dridco.imagestore.model;

import dridco.imagestore.model.filters.ImageFilterException;

import java.io.IOException;

public interface ImageFilter {

    byte[] filter(byte[] content) throws IOException, ImageFilterException;
}
