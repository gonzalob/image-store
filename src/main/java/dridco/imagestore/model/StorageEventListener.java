package dridco.imagestore.model;

public interface StorageEventListener {

	void loaded(Image image);
	void referenced(Image image);
	void stored(Image image);
	void released(Identifier identifier);
	void removed(Image image);
}
