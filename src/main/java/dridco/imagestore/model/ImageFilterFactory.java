package dridco.imagestore.model;

import dridco.imagestore.model.filters.ImageFilterFactoryException;

public interface ImageFilterFactory {

	ImageFilter create(String settings) throws ImageFilterFactoryException;
}
