package dridco.imagestore.model;

import com.google.common.base.Predicate;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

public class DistinctHashes implements Predicate<Image> {

	private Set<String> hashes = newHashSet();

	public static DistinctHashes distinctHashes() {
		return new DistinctHashes();
	}

	@Override
	public boolean apply(Image input) {
		return hashes.add(input.getEntityTag());
	}

}
