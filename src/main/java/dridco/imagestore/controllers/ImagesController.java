package dridco.imagestore.controllers;

import com.google.common.base.Optional;
import dridco.imagestore.model.*;
import dridco.imagestore.model.filters.DecoderNotFound;
import dridco.imagestore.model.filters.ImageFilterException;
import dridco.imagestore.model.filters.ImageFilterFactoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.EOFException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.net.HttpHeaders.*;
import static dridco.imagestore.controllers.Constants.*;
import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.DAYS;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(ROOT_PATH)
public class ImagesController {

	public  static final String ANY_HASH = "\"*\"";
	private static final Object DEFAULT_CACHE_MAX_AGE = DAYS.toSeconds(365);
	private static final String IMAGES = "image/*";

	private Clients clients;
	private CommandFactory commands;
	private Storage storage;

	private Object cacheMaxAge;
	private FilterDefinitionParser filters;

	@Autowired
	public ImagesController(Clients clients, CommandFactory commands, Storage storage, FilterDefinitionParser filters) {
		cacheMaxAge = DEFAULT_CACHE_MAX_AGE;
		this.clients = checkNotNull(clients);
		this.commands = checkNotNull(commands);
		this.storage = checkNotNull(storage);
		this.filters = filters;
	}

	@RequestMapping(method = GET)
	@ResponseStatus(NO_CONTENT)
	public void root() {
	}

	@RequestMapping(method = POST, consumes = IMAGES)
	public ResponseEntity<Void> post(
			@RequestHeader(value = CLIENT_KEY_HEADER, required = false) String clientKey,
			@RequestHeader(CONTENT_TYPE) MediaType type,
			@RequestBody byte[] content) throws ImageStoreException, URISyntaxException, MalformedURLException {
		Optional<Client> maybeClient = clients.get(clientKey);
		if(maybeClient.isPresent()) {
			String identifier = UUID.randomUUID().toString();
			Client client = maybeClient.get();
			Image image = storage.execute(commands.store(
					client,
					new MagicNumberCandidate(type, content),
					identifier,
					client.publicUrl(identifier)
			));
			return created(image);
		} else {
			return new ResponseEntity<>(UNAUTHORIZED);
		}
	}

	@RequestMapping(value = "{id}", method = GET, produces = IMAGES)
	public ResponseEntity<byte[]> id(
			@PathVariable String id,
			@RequestParam(value = TRANSFORM_PARAM, defaultValue = EMPTY) List<String> filterChain,
			@RequestHeader(value = IF_NONE_MATCH, defaultValue = EMPTY) String eTags) throws Exception {
        return load(id, eTags, new FilteredImage(filterChain));
	}

	@RequestMapping(value = "{id}", method = HEAD, produces = IMAGES)
	public ResponseEntity<byte[]> metadata(
			@PathVariable String id,
			@RequestHeader(value = IF_NONE_MATCH, defaultValue = EMPTY) String eTags) throws Exception {
        return load(id, eTags, new IgnoreBody());
	}

    private ResponseEntity<byte[]> load(
            @PathVariable String id,
            @RequestHeader(value = IF_NONE_MATCH, defaultValue = EMPTY) String eTags,
            BodyFilter bodyFilter) throws Exception {
        Image image = storage.execute(commands.get(id));

        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl("public, max-age=" + cacheMaxAge);
        headers.setETag(image.getEntityTag());
        if (image.entityTagMatches(eTags)) {
            return new ResponseEntity<>(headers, NOT_MODIFIED);
        } else {
            byte[] content = bodyFilter.process(image);
            return new ResponseEntity<>(content, headers, OK);
        }
    }

    private interface BodyFilter {

        byte[] process(Image image) throws ImageFilterFactoryException, IOException, ImageFilterException;
    }

    private static class IgnoreBody implements BodyFilter {

        @Override
        public byte[] process(Image image) {
            return new byte[0];
        }
    }

    private class FilteredImage implements BodyFilter {

        private List<String> filterChain;

        public FilteredImage(List<String> filterChain) {
            this.filterChain = filterChain;
        }

        @Override
        public byte[] process(Image image) throws ImageFilterFactoryException, IOException, ImageFilterException {
			try {
				byte[] rawContent = image.getContent();
				return filters.parse(filterChain).filter(rawContent);
			} catch(ImageFilterException e) {
				e.setIdentifier(image.getIdentifier());
                throw e;
			}
        }
    }

	@RequestMapping(value = "{id}", method = PUT, consumes = IMAGES)
	public ResponseEntity<Void> put(@PathVariable String id,
			Principal principal,
			@RequestHeader(CONTENT_TYPE) MediaType type,
			@RequestBody byte[] image) throws ImageStoreException, URISyntaxException, MalformedURLException {
		Client client = clientFromPrincipal(principal);
		return created(storage.execute(commands.store(
                client,
                new MagicNumberCandidate(type, image),
                id,
                client.publicUrl(id))
        ));
	}

	@RequestMapping(value = "{id}", method = PUT, consumes = HASH_CONTENT_TYPE_VALUE)
	public ResponseEntity<Void> alias(@PathVariable String id,
			Principal principal,
			@RequestBody String hash) throws ImageStoreException, URISyntaxException, MalformedURLException {
		Client client = clientFromPrincipal(principal);
		return created(storage.execute(commands.alias(client, id, hash, client.publicUrl(id))));
	}

	@RequestMapping(value = "{id}", method = DELETE)
	public ResponseEntity<Void> delete(@PathVariable String id,
			Principal principal,
			@RequestHeader(IF_MATCH) String eTags) throws ImageStoreException {
		Hash ifMatch = hashFrom(eTags);
		storage.execute(commands.release(clientFromPrincipal(principal), id, ifMatch));
		return new ResponseEntity<>(OK);
	}

	private Hash hashFrom(String eTags) {
		if(ANY_HASH.equals(eTags)) {
			return new AnyHash();
		} else {
			return new EntityTagsRequestParameter(asList(eTags.split(",")));
		}
	}

	@ExceptionHandler(IdentifierExists.class)
	@ResponseStatus(CONFLICT)
	public void conflict() {}

	@ExceptionHandler(HashMismatch.class)
	@ResponseStatus(PRECONDITION_FAILED)
	public void preconditionFailed() {}

	@ExceptionHandler(IdentifierDoesNotExist.class)
	@ResponseStatus(NOT_FOUND)
	public void notFound() {}

	@ExceptionHandler(DoesNotBelongToRequester.class)
	@ResponseStatus(FORBIDDEN)
	public void forbidden() {}

	@ExceptionHandler(IdentifierDeleted.class)
	@ResponseStatus(GONE)
	public void gone() {}

	@ExceptionHandler(EOFException.class)
    @ResponseStatus(BAD_REQUEST)
	public void ignoreClientDisconnections() {}

	@ExceptionHandler({UnsupportedContentType.class, ImageFilterFactoryException.class})
    @ResponseStatus(BAD_REQUEST)
	public void illegalOrUnexpectedInput() {}

	@ExceptionHandler(DecoderNotFound.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
	public void decoderNotFound() {}

	private ResponseEntity<Void> created(Image image) throws URISyntaxException {
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(image.getIdentifier().getPublicUrl().toURI());
		headers.setETag(image.getEntityTag());
		return new ResponseEntity<>(headers, CREATED);
	}

	private Client clientFromPrincipal(Principal principal) {
		return clients.get(principal.getName()).get();
	}
}
