package dridco.imagestore.controllers;

public interface Constants {

	String ROOT_PATH               = "/";
	String CLIENT_KEY_HEADER       = "X-ImageStore-ClientKey";
	String HASH_CONTENT_TYPE_VALUE = "text/vnd.imagestore.hash";
	String TRANSFORM_PARAM         = "t";
}
