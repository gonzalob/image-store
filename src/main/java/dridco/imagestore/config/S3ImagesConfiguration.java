package dridco.imagestore.config;

import com.amazonaws.regions.Regions;
import dridco.imagestore.model.s3.S3Images;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty({
	S3ImagesConfiguration.S3_API_KEY,
	S3ImagesConfiguration.S3_SECRET,
	S3ImagesConfiguration.S3_BUCKET,
	S3ImagesConfiguration.S3_REGION
 })
public class S3ImagesConfiguration {

	protected static final String S3_API_KEY = "s3.key";
	protected static final String S3_SECRET = "s3.secret";
	protected static final String S3_BUCKET = "s3.bucket";
	protected static final String S3_REGION = "s3.region";

	private static final Logger LOG = LoggerFactory.getLogger(S3ImagesConfiguration.class);

	@Value("${" + S3_API_KEY + "}")
	private String apiKey;
	@Value("${" + S3_SECRET + "}")
	private String secret;
	@Value("${" + S3_BUCKET + "}")
	private String bucketName;
	@Value("${" + S3_REGION + "}")
	private String regionName;

	@Bean
	public S3Images images() {
		Regions region = Regions.valueOf(regionName);
		LOG.info("Images will be stored in S3, under bucket {}", bucketName);
		return new S3Images(apiKey, secret, region, bucketName);
	}
} 