package dridco.imagestore.config;

import dridco.imagestore.controllers.Constants;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = Constants.class)
public class RestConfiguration {
}
