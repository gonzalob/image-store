package dridco.imagestore.config;

import dridco.imagestore.model.*;
import dridco.imagestore.model.filters.FailSafeFilterFactory;
import dridco.imagestore.model.filters.ResizeFactory;
import dridco.imagestore.model.inmemory.CachedClients;
import dridco.imagestore.model.jdbc.JdbcClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;

@Configuration
@ComponentScan
@EnableScheduling
public class ModelConfiguration {

	private static final Logger LOG = LoggerFactory.getLogger(ModelConfiguration.class);

	@Value("${filters.fail-safe:false}")
	private boolean failSafeFilters;

	@Autowired
	@Bean
	public Storage storage(Identifiers identifiers, Images images, Clock clock) {
		return new Storage(identifiers, images, clock);
	}

	@Autowired
	@Bean
	public CommandFactory commandFactory(List<StorageEventListener> listeners) {
		LOG.info("Objects interested in Storage events: {}", listeners);
		return new CommandFactory(new DemultiplexerStorageEventListener(listeners));
	}

	@Autowired
	@Bean
	public Clients clients(JdbcTemplate template) {
		return new CachedClients(new JdbcClients(template));
	}

	@ConditionalOnProperty("scheduled-tasks.enabled")
	@Autowired
	@Bean
	public GarbageCollectorTask garbageCollectorScheduling(GarbageCollector garbageCollector, Clock clock) {
		return new GarbageCollectorTask(garbageCollector, clock);
	}

	@Autowired
	@Bean
	public GarbageCollector garbageCollector(Clients clients, Identifiers identifiers, Images images, IdentifierValidator identifierValidator, List<StorageEventListener> listener) {
		return new GarbageCollector(clients, identifiers, images, identifierValidator, new DemultiplexerStorageEventListener(listener));
	}
	
	@Autowired
	@Bean
	public IdentifierValidator identifierValidator(Clock clock) {
		return new HttpClientIdentifierValidator(httpClient(), clock);
	}

	@Bean
	public FilterRegistry registry() {
        LOG.info("Fail safe filters {}", failSafeFilters ? "enabled" : "disabled");
		return new FilterRegistryBuilder()
			.addFilter("r", failSafeIfEnabled(new ResizeFactory())).build();
	}

	private ImageFilterFactory failSafeIfEnabled(ImageFilterFactory wrapped) {
	    return failSafeFilters ? new FailSafeFilterFactory(wrapped) : wrapped;
	}

	private CloseableHttpClient httpClient() {
		return HttpClientBuilder.create().build();
	}

	@Bean
	public Clock clock() {
		return new SystemClock();
	}
}
