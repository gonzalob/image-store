package dridco.imagestore.config;

import dridco.imagestore.model.gcs.GoogleCloudStorageImages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static com.google.api.services.storage.StorageScopes.DEVSTORAGE_READ_WRITE;
import static com.google.auth.oauth2.GoogleCredentials.getApplicationDefault;
import static com.google.cloud.storage.StorageOptions.newBuilder;
import static dridco.imagestore.config.GoogleCloudStorageImagesConfiguration.CLOUD_STORAGE_BUCKET_NAME;
import static dridco.imagestore.config.GoogleCloudStorageImagesConfiguration.GOOGLE_CLOUD_PROJECT_NAME;
import static java.util.Collections.singletonList;

@Configuration
@ConditionalOnProperty({
        GOOGLE_CLOUD_PROJECT_NAME,
        CLOUD_STORAGE_BUCKET_NAME
})
public class GoogleCloudStorageImagesConfiguration {

    static final String GOOGLE_CLOUD_PROJECT_NAME = "gcs.project";
    static final String CLOUD_STORAGE_BUCKET_NAME = "gcs.bucket";

    private static final Logger LOG = LoggerFactory.getLogger(GoogleCloudStorageImagesConfiguration.class);

    @Value("${" + GOOGLE_CLOUD_PROJECT_NAME + "}")
    private String project;
    @Value("${" + CLOUD_STORAGE_BUCKET_NAME + "}")
    private String bucketName;

    @Bean
    public GoogleCloudStorageImages images() throws GeneralSecurityException, IOException {
        LOG.info("Images will be stored in Google Cloud Storage, under bucket {}", bucketName);
        return new GoogleCloudStorageImages(
                newBuilder()
                        .setProjectId(project)
                        .setCredentials(getApplicationDefault().createScoped(singletonList(DEVSTORAGE_READ_WRITE)))
                        .build()
                , bucketName);
    }
}
