package dridco.imagestore.config;

import dridco.imagestore.model.Clock;

import java.util.Date;

public class SystemClock implements Clock {

	@Override
	public Date now() {
		return new Date();
	}
}
