package dridco.imagestore.config;

import dridco.imagestore.model.Slf4jLoggingStorageEventListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StorageEventListeners {

	@Bean
	public Slf4jLoggingStorageEventListener logging() {
		return new Slf4jLoggingStorageEventListener();
	}
}
