package dridco.imagestore.config;

import dridco.imagestore.model.Clients;
import dridco.imagestore.model.Identifiers;
import dridco.imagestore.model.jdbc.JdbcIdentifiers;
import liquibase.exception.LiquibaseException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.slf4j.LoggerFactory.getLogger;

@Configuration
public class JdbcIdentifiersConfiguration {

	private static final Logger LOG = getLogger(JdbcIdentifiersConfiguration.class);

	@Autowired
	@Bean
	public Identifiers identifiers(DataSource dataSource, Clients clients) throws SQLException, LiquibaseException {
		LOG.info("Identifiers will be stored in SQL");
		return new JdbcIdentifiers(dataSource, clients);
	}
}