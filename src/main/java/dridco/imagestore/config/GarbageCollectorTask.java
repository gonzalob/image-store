package dridco.imagestore.config;

import dridco.imagestore.model.Clock;
import dridco.imagestore.model.GarbageCollector;
import dridco.imagestore.model.IdentifiersException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

public class GarbageCollectorTask {

	private static final long SECS_IN_A_MIN = 60;
	private static final long MILLIS_IN_A_SEC = 1000;
	private static final long ONE_MINUTE = SECS_IN_A_MIN * MILLIS_IN_A_SEC;
	private static final long DELAY_BETWEEN_GCS = ONE_MINUTE;
	private static final long INITIAL_DELAY = ONE_MINUTE;

	private static final Logger LOG = LoggerFactory.getLogger(GarbageCollectorTask.class);
	private GarbageCollector collector;
	private Clock clock;

	public GarbageCollectorTask(GarbageCollector collector, Clock clock) {
		LOG.info("Image identifiers will be validated every {} millis, " +
				"after an initial delay of {} millis", DELAY_BETWEEN_GCS, INITIAL_DELAY);
		this.collector = collector;
		this.clock = clock;
	}

	@Scheduled(initialDelay = INITIAL_DELAY, fixedDelay = DELAY_BETWEEN_GCS)
	public void collectGarbage() throws IdentifiersException {
		Date now = clock.now();
		collector.execute(now);
	}
}
