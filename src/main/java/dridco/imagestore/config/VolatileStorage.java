package dridco.imagestore.config;

import dridco.imagestore.model.Identifiers;
import dridco.imagestore.model.Images;
import dridco.imagestore.model.inmemory.InMemoryIdentifiers;
import dridco.imagestore.model.inmemory.InMemoryImages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VolatileStorage {

	private static final Logger LOG = LoggerFactory.getLogger(VolatileStorage.class);

	@Bean
    @ConditionalOnMissingBean(Identifiers.class)
	public Identifiers identifiers() {
        LOG.warn("Using in-memory identifiers");
		return new InMemoryIdentifiers();
	}

	@Bean
    @ConditionalOnMissingBean(Images.class)
	public Images images() {
        LOG.warn("Using in-memory images");
		return new InMemoryImages();
	}
}
