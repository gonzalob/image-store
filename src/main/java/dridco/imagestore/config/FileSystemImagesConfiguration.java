package dridco.imagestore.config;

import dridco.imagestore.model.Images;
import dridco.imagestore.model.filesystem.FileSystemImages;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.nio.file.Path;

import static org.slf4j.LoggerFactory.getLogger;

@Configuration
@ConditionalOnProperty("images.root")
public class FileSystemImagesConfiguration {

	private static final Logger LOG = getLogger(FileSystemImagesConfiguration.class);

	@Value("${images.root}")
	private File root;

	@Bean
	public Images images() {
		Path root = root();
		LOG.info("Images will be stored in the file system, at " + root);
		return new FileSystemImages(root);
	}

	private Path root() {
		if (root.mkdirs()) {
			LOG.warn("Created images root directory " + root);
		}
		return root.toPath();
	}
}