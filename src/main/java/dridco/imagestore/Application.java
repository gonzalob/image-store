package dridco.imagestore;

import dridco.imagestore.config.ModelConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import(ModelConfiguration.class)
public class Application {

	private static final Class<Application> APPLICATION = Application.class;

	public static void main(String[] args) {
		SpringApplication.run(APPLICATION, args);
	}
}
