package dridco.imagestore;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;

import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@ActiveProfiles("test")
@IntegrationTest("server.port:0")
public class BootstrapTest {

	@Autowired
	private EmbeddedWebApplicationContext server;
	
	@Value("${local.server.port}")
	private int port;

	private HttpClient client = HttpClientBuilder.create().build();

	@Test
	@DirtiesContext
	public void thatTheApplicationIsSuccessfullyBootstrapped() throws IOException {
		HttpResponse response = client.execute(new HttpGet("http://localhost:" + port));
		assertEquals(SC_NO_CONTENT, response.getStatusLine().getStatusCode());
	}
}
