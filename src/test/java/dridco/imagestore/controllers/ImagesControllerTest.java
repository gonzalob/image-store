package dridco.imagestore.controllers;

import dridco.imagestore.Application;
import dridco.imagestore.config.StubFilterConfiguration;
import dridco.imagestore.model.Identifiers;
import dridco.imagestore.model.ImageBuilder;
import dridco.imagestore.model.ImageRequests;
import dridco.imagestore.model.filters.StubFilterFactory;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.mvc.condition.MediaTypeExpression;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.Filter;
import java.net.URI;

import static com.google.common.net.HttpHeaders.*;
import static dridco.imagestore.config.StubFilterConfiguration.*;
import static dridco.imagestore.controllers.Constants.HASH_CONTENT_TYPE_VALUE;
import static dridco.imagestore.controllers.Constants.TRANSFORM_PARAM;
import static dridco.imagestore.controllers.ImagesController.ANY_HASH;
import static dridco.imagestore.model.ImageBuilder.*;
import static dridco.imagestore.model.ImageRequests.post;
import static dridco.imagestore.model.ImageRequests.secured;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.HttpMethod.HEAD;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, StubFilterConfiguration.class})
@WebAppConfiguration
@ActiveProfiles("test")
public class ImagesControllerTest {

	public static final byte[] EMPTY_RESPONSE = new byte[0];
	@Autowired
	private StubFilterFactory stubFilter;
    @Autowired
    private Identifiers identifiers;
	private MockMvc mvc;

	@Autowired
	public void setup(WebApplicationContext ctx, @Qualifier("springSecurityFilterChain") Filter springSecurityFilter) {
		mvc = webAppContextSetup(ctx).addFilters(springSecurityFilter).build();
	}

	@After
	public void cleanup() throws Exception {
		mvc.perform(ImageRequests.delete());
        identifiers.purge();
	}

	@Test
	public void thatRetrievingAnInexistingImageReturnsNotFound() throws Exception {
		mvc.perform(get("inexisting_image"))
			.andExpect(status().isNotFound());
	}

	@Test
	public void thatPostingAnImageStoresIt() throws Exception {
		mvc.perform(post())
			.andExpect(status().isCreated());
	}

	@Test
	public void thatNewImagesPointToTheirLocation() throws Exception {
		mvc.perform(post())
			.andExpect(header().string(LOCATION, not(isEmptyOrNullString())));
	}

	@Test
	public void thatGettingAnImageDumpsItOnTheResponse() throws Exception {
		mvc.perform(post()).andDo(new ResultHandler() {

            @Override
            public void handle(MvcResult result) throws Exception {
                String location = result.getResponse().getHeader(LOCATION);
                mvc.perform(get(location).accept(IMAGE_JPEG))
                        .andExpect(content().bytes(ImageBuilder.DEFAULT_CONTENT));
            }
        });
	}

	@Test
	public void thatPuttingAnImageStoresItUnderThatUri() throws Exception {
		mvc.perform(ImageRequests.put());
		mvc.perform(ImageRequests.get())
			.andExpect(content().bytes(ImageBuilder.DEFAULT_CONTENT));
	}

	@Test
	public void thatPuttingAnImageOnAnExistingUriRejectsIt() throws Exception {
		mvc.perform(ImageRequests.put())
			.andExpect(status().isCreated());
		mvc.perform(ImageRequests.put())
			.andExpect(status().isConflict());
	}

	@Test
	public void thatDeletingAnImageReleasesTheUri() throws Exception {
		mvc.perform(ImageRequests.put());
		mvc.perform(ImageRequests.delete())
			.andExpect(status().isOk());
		mvc.perform(ImageRequests.get())
			.andExpect(status().isGone());
	}

	@Test
	public void thatGettingAnImageContainsItsEtag() throws Exception {
        thatRequestingAnImageContainsItsEtag(ImageRequests.get());
	}

	@Test
	public void thatDeletingAnImageRequiresItsEntityTag() throws Exception {
		mvc.perform(ImageRequests.put());
		mvc.perform(secured(delete(ImageRequests.path())))
			.andExpect(status().isBadRequest());
	}

	@Test
	public void thatProvidingTheWrongEntityTagRejectsDeletion() throws Exception {
		mvc.perform(ImageRequests.put());
		mvc.perform(deleteWithInvalidEntityTag())
			.andExpect(status().isPreconditionFailed());
	}

	@Test
	public void thatGettingAnImageKnowingItsEntityTagReturnsNotModified() throws Exception {
        thatRequestingAnImageKnowingItsEntityTagReturnsNotModified(ImageRequests.get());
	}

	@Test
	public void thatGettingAnImageWithWrongEntityTagReturnsTheCurrent() throws Exception {
        thatRequestingAnImageWithWrongEntityTagReturnsTheCurrent(ImageRequests.get());
	}

	@Test
	public void thatDoesNotAcceptRequestsForContentsThatAreNotImages() throws Exception {
		mvc.perform(ImageRequests.put());
		mvc.perform(get(ImageRequests.path()).accept(TEXT_PLAIN))
			.andExpect(status().isNotAcceptable());
	}

	@Test
	public void thatItRequiresClientIdentifierToAcceptPosts() throws Exception {
		mvc.perform(ImageRequests.postWithoutIdentification())
			.andExpect(status().isUnauthorized());
	}

	@Test
	public void thatItRequiresAuthenticationForPutting() throws Exception {
		mvc.perform(ImageRequests.putWithoutIdentification())
			.andExpect(status().isUnauthorized());
	}

	@Test
	public void thatItRequiresAuthenticationForDeleting() throws Exception {
		mvc.perform(ImageRequests.postWithoutIdentification())
			.andExpect(status().isUnauthorized());
	}

	@Test
	public void thatAUserCannotDeleteAnotherUsersImage() throws Exception {
		mvc.perform(ImageRequests.put())
			.andExpect(status().isCreated());
		mvc.perform(ImageRequests.deleteWithoutIdentification().header(AUTHORIZATION, "Basic dXNlcjpwYXNzd29yZA=="))
			.andExpect(status().isForbidden());
	}

	@Test
	public void thatItDumpsCacheHeaders() throws Exception {
		mvc.perform(ImageRequests.put())
			.andExpect(status().isCreated());
		mvc.perform(ImageRequests.get())
			.andExpect(header().string(CACHE_CONTROL, "public, max-age=31536000"));
	}

	@Test
	public void thatItAcceptsDeletingWithAsteriskAsEntityTag() throws Exception {
		mvc.perform(ImageRequests.put())
			.andExpect(status().isCreated());
		mvc.perform(secured(delete(ImageRequests.path()).header(IF_MATCH, ANY_HASH)))
			.andExpect(status().isOk());
	}

	@Test
	public void thatItCanAliasAnExistingImage() throws Exception {
		URI alias = UriComponentsBuilder.fromUriString("/{0}").buildAndExpand("alias").toUri();
		mvc.perform(ImageRequests.put())
			.andExpect(status().isCreated());
		mvc.perform(secured(put(alias).contentType(parseMediaType(HASH_CONTENT_TYPE_VALUE)).content(DEFAULT_HASH)))
			.andExpect(status().isCreated());
		mvc.perform(get(alias).accept(IMAGE_JPEG))
			.andExpect(content().bytes(DEFAULT_CONTENT));
	}

	@Test
	public void thatItFailsToAliasAnImageWhenTheSourceIsUnknown() throws Exception {
		mvc.perform(secured(put(DEFAULT_ID)).contentType(parseMediaType(HASH_CONTENT_TYPE_VALUE)).content(DEFAULT_HASH))
			.andExpect(status().isNotFound());
	}

	@Test
	public void thatItAppliesFiltersWhenRequired() throws Exception {
		mvc.perform(ImageRequests.put())
			.andExpect(status().isCreated());
		mvc.perform(ImageRequests.get().param(TRANSFORM_PARAM, FILTER_KEY + SPLITTER + EXPECTED_CONFIG))
			.andExpect(status().isOk());

		assertTrue(stubFilter.invoked());
	}

	@Test
	public void thatHeadingExistingUrlHasNoContent() throws Exception {
		mvc.perform(post()).andDo(new ResultHandler() {

            @Override
            public void handle(MvcResult result) throws Exception {
                String location = result.getResponse().getHeader(LOCATION);
                mvc.perform(request(HEAD, location).accept(IMAGE_JPEG))
                        .andExpect(content().bytes(EMPTY_RESPONSE));
            }
        });
	}

    @Test
    public void thatHeadingExistingUrlIsOk() throws Exception {
        mvc.perform(post()).andDo(new ResultHandler() {

            @Override
            public void handle(MvcResult result) throws Exception {
                String location = result.getResponse().getHeader(LOCATION);
                mvc.perform(request(HEAD, location).accept(IMAGE_JPEG))
                        .andExpect(status().isOk());
            }
        });
    }

    @Test
    public void thatHeadingAnImageContainsItsEtag() throws Exception {
        thatRequestingAnImageContainsItsEtag(ImageRequests.head());
    }

    @Test
    public void thatHeadingAnImageKnowingItsEntityTagReturnsNotModified() throws Exception {
        thatRequestingAnImageKnowingItsEntityTagReturnsNotModified(ImageRequests.head());
    }

    @Test
    public void thatHeadingAnImageWithWrongEntityTagReturnsTheCurrent() throws Exception {
        thatRequestingAnImageWithWrongEntityTagReturnsTheCurrent(ImageRequests.head());
    }

	@Test
    public void thatUploadingDataWithoutMatchingContentTypeFails() throws Exception {
        mvc.perform(post().contentType(IMAGE_JPEG).content("GIF8"))
                .andExpect(status().isBadRequest());
    }

    private void thatRequestingAnImageContainsItsEtag(MockHttpServletRequestBuilder request) throws Exception {
        mvc.perform(ImageRequests.put());
        mvc.perform(request)
                .andExpect(header().string(ETAG, "\"" + ImageBuilder.DEFAULT_HASH + "\""));
    }

    private void thatRequestingAnImageKnowingItsEntityTagReturnsNotModified(MockHttpServletRequestBuilder request) throws Exception {
        mvc.perform(ImageRequests.put());
        mvc.perform(request.header(IF_NONE_MATCH, "\"" + ImageBuilder.DEFAULT_HASH + "\""))
                .andExpect(status().isNotModified());
    }

    private void thatRequestingAnImageWithWrongEntityTagReturnsTheCurrent(MockHttpServletRequestBuilder request) throws Exception {
        mvc.perform(ImageRequests.put());
        mvc.perform(request.header(ETAG, reverse("\"" + ImageBuilder.DEFAULT_HASH + "\"")))
                .andExpect(status().isOk());
    }

	private MockHttpServletRequestBuilder deleteWithInvalidEntityTag() {
		return secured(delete(ImageRequests.path()).header(IF_MATCH, reverse("\"" + ImageBuilder.DEFAULT_HASH + "\"")));
	}
}
