package dridco.imagestore.model;

import dridco.imagestore.model.filters.ImageFilterException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

public abstract class ImageFilterTest {

	private static final String DEBUG_TARGET_PROPERTY = "image-filter-test.output";

	private static final Logger LOG = LoggerFactory.getLogger(ImageFilterTest.class);
	private static final String SAMPLE = "sample";
	protected static final Integer SAMPLE_WIDTH  = 100;
	protected static final Integer SAMPLE_HEIGHT = 100;
	protected static final String ORIGINAL_JPG = SAMPLE + ".jpg";
	protected static final String ORIGINAL_GIF = SAMPLE + ".gif";
	protected static final String ORIGINAL_PNG = SAMPLE + ".png";

	private ImageFilter tested;

	protected abstract ImageFilter create();
	protected abstract String expectedPngFilename();
	protected abstract String expectedGifFilename();
	protected abstract String expectedJpegFilename();

	@Before
	public final void setUp() {
		tested = create();
	}

	@Test
	public void thatItCanApplyOnPng() throws IOException, URISyntaxException, ImageFilterException {
		thatItCanApplyOn(loadPng(), expectedPng(), "png");
	}

	@Test
	public void thatItCanApplyOnGif() throws IOException, URISyntaxException, ImageFilterException {
		thatItCanApplyOn(loadGif(), expectedGif(), "gif");
	}

	@Test
	public void thatItCanApplyOnJpeg() throws IOException, URISyntaxException, ImageFilterException {
		thatItCanApplyOn(loadJpeg(), expectedJpeg(), "jpg");
	}

	private void thatItCanApplyOn(byte[] sample, byte[] expected, String extension) throws IOException, ImageFilterException {
		byte[] actual = tested.filter(sample);
		dumpContentsIfDebugEnabled(extension, actual);
		assertArrayEquals(expected, actual);
	}

	private void dumpContentsIfDebugEnabled(String extension, byte[] result) throws IOException {
		String target = System.getProperty(DEBUG_TARGET_PROPERTY);
		if(isNotEmpty(target)) {
			dumpContents(target, extension, result);
			LOG.info("Dumped filter results in {}", target);
		} else {
			LOG.debug("Target for storing filter results is undefined, not executing. "
					+ "To enable, set the {} system property", DEBUG_TARGET_PROPERTY);
		}
	}
	private void dumpContents(String target, String extension, byte[] result) throws IOException {
		File outputDir = new File(target);
		assertTrue(format("Dumping filter output is enabled but defined target %s is not a directory", target), outputDir.isDirectory());
		try(FileOutputStream out = new FileOutputStream(new File(target, getClass().getSimpleName() + "." + extension))) {
			out.write(result);
		}
	}

	private byte[] expectedPng() throws IOException, URISyntaxException {
		return load(expectedPngFilename());
	}

	private byte[] expectedGif() throws IOException, URISyntaxException {
		return load(expectedGifFilename());
	}

	private byte[] expectedJpeg() throws IOException, URISyntaxException {
		return load(expectedJpegFilename());
	}

	private byte[] loadPng() throws IOException, URISyntaxException {
		return load(ORIGINAL_PNG);
	}

	private byte[] loadGif() throws IOException, URISyntaxException {
		return load(ORIGINAL_GIF);
	}

	private byte[] loadJpeg() throws IOException, URISyntaxException {
		return load(ORIGINAL_JPG);
	}

	private byte[] load(String resource) throws IOException, URISyntaxException {
		URL url = getClass().getResource("/" + resource);
		File file = new File(url.toURI());
		return Files.readAllBytes(file.toPath());
	}
}
