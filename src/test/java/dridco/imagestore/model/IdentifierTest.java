package dridco.imagestore.model;

import org.junit.Test;

import static dridco.imagestore.model.IdentifierBuilder.*;

public class IdentifierTest {

	@Test(expected = NullPointerException.class)
	public void thatAnIdentifierCannotBeBuiltWithoutAnOwner() {
		anIdentifier().identifiedBy(DEFAULT_ID).withHash(DEFAULT_HASH).createdOn(DEFAULT_CREATION_DATE).build();
	}

	@Test(expected = NullPointerException.class)
	public void thatAnIdentifierCannotBeBuiltWithoutAnId() {
		anIdentifier().ownedBy(DEFAULT_OWNER).withHash(DEFAULT_HASH).createdOn(DEFAULT_CREATION_DATE).build();
	}

	@Test(expected = NullPointerException.class)
	public void thatAnIdentifierCannotBeBuiltWithoutItsHash() {
		anIdentifier().ownedBy(DEFAULT_OWNER).identifiedBy(DEFAULT_ID).createdOn(DEFAULT_CREATION_DATE).build();
	}

	@Test(expected = NullPointerException.class)
	public void thatAnIdentifierCannotBeBuiltWithoutItsCreationDate() {
		anIdentifier().ownedBy(DEFAULT_OWNER).identifiedBy(DEFAULT_ID).withHash(DEFAULT_HASH).build();
	}
}
