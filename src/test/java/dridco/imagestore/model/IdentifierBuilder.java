package dridco.imagestore.model;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;

import static dridco.imagestore.model.ClientBuilder.aClient;
import static dridco.imagestore.model.Dates.parse;

public final class IdentifierBuilder {

	public static final Client DEFAULT_OWNER = aClient().withDefaults().build();
	public static final String DEFAULT_ID = "id";
	public static final String DEFAULT_HASH = "hash";
	public static final Date DEFAULT_CREATION_DATE = defaultDate();
	public static final Date DEFAULT_VALIDATION_DATE = DEFAULT_CREATION_DATE;
	public static final URL DEFAULT_PUBLIC_URL = defaultUrl();

	private static Date defaultDate() {
		try {
			return parse("2014-09-25 18:12:26");
		} catch (ParseException e) {
			throw new AssertionError("ParseException with a fixed pattern and argument");
		}
	}

	private static URL defaultUrl() {
		try {
			return new URL("http://localhost/sample_image");
		} catch (MalformedURLException e) {
			throw new AssertionError("MalformedURLException with a fixed String representing a valid URL");
		}
	}

	private Client owner;
	private String id;
	private String hash;
	private URL publicUrl;
	private Date creation;
	private Date validation;

	public static IdentifierBuilder anIdentifier() {
		return new IdentifierBuilder();
	}

	public IdentifierBuilder withDefaults() {
		owner = DEFAULT_OWNER;
		id = DEFAULT_ID;
		hash = DEFAULT_HASH;
		creation = DEFAULT_CREATION_DATE;
		validation = DEFAULT_VALIDATION_DATE;
		publicUrl = DEFAULT_PUBLIC_URL;
		return this;
	}

	public IdentifierBuilder identifiedBy(String id) {
		this.id = id;
		return this;
	}

	public IdentifierBuilder withHash(String hash) {
		this.hash = hash;
		return this;
	}

	public IdentifierBuilder ownedBy(Client owner) {
		this.owner = owner;
		return this;
	}

	public IdentifierBuilder createdOn(Date when) {
		creation = when;
		return this;
	}

	public IdentifierBuilder validatedOn(Date when) {
		validation = when;
		return this;
	}

	public Identifier build() {
		return new Identifier(owner, id, publicUrl, hash, creation, validation);
	}
}
