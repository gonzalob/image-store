package dridco.imagestore.model.inmemory;

import dridco.imagestore.model.Images;
import dridco.imagestore.model.ImagesTest;

public class InMemoryImagesTest extends ImagesTest {

	@Override
	protected Images create() {
		return new InMemoryImages();
	}
}
