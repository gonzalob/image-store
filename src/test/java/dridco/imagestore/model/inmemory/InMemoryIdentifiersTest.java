package dridco.imagestore.model.inmemory;

import dridco.imagestore.model.Identifiers;
import dridco.imagestore.model.IdentifiersTest;

public class InMemoryIdentifiersTest extends IdentifiersTest {

	@Override
	public Identifiers create() {
		return new InMemoryIdentifiers();
	}
}
