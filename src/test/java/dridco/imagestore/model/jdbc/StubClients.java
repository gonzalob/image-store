package dridco.imagestore.model.jdbc;

import com.google.common.base.Optional;
import dridco.imagestore.model.Client;
import dridco.imagestore.model.Clients;

import java.util.Collection;
import java.util.List;

import static com.google.common.collect.Iterables.tryFind;
import static dridco.imagestore.model.jdbc.ClientWithKey.clientWithKey;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableCollection;

public class StubClients implements Clients {

	private List<Client> clients;

	public StubClients(Client... clients) {
		this.clients = asList(clients);
	}

	@Override
	public Optional<Client> get(String clientKey) {
		return tryFind(clients, clientWithKey(clientKey));
	}

	@Override
	public Collection<Client> getAll() {
		return unmodifiableCollection(clients);
	}
}
