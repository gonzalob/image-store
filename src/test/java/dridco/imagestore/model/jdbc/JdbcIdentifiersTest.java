package dridco.imagestore.model.jdbc;

import dridco.imagestore.model.ClientBuilder;
import dridco.imagestore.model.Clients;
import dridco.imagestore.model.Identifiers;
import dridco.imagestore.model.IdentifiersTest;
import liquibase.exception.LiquibaseException;
import org.junit.After;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import java.sql.SQLException;

public class JdbcIdentifiersTest extends IdentifiersTest {

	private EmbeddedDatabase database;

	@Override
	public Identifiers create() throws SQLException, LiquibaseException {
		database = new EmbeddedDatabaseBuilder().build();

		JdbcIdentifiers identifiers = new JdbcIdentifiers(database, clients());
		identifiers.createLockFor(OWNER);
		return identifiers;
	}

	private Clients clients() {
		return new StubClients(OWNER);
	}

	@After
	public void tearDown() {
		database.shutdown();
	}
}
