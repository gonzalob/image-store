package dridco.imagestore.model.jdbc;

import com.google.common.base.Predicate;
import dridco.imagestore.model.Client;

import static com.google.common.base.Preconditions.checkNotNull;

public class ClientWithKey implements Predicate<Client> {

	public static ClientWithKey clientWithKey(String key) {
		return new ClientWithKey(key);
	}

	private String key;

	public ClientWithKey(String key) {
		this.key = checkNotNull(key);
	}

	@Override
	public boolean apply(Client input) {
		return key.equals(input.toString());
	}
}
