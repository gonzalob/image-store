package dridco.imagestore.model;

import org.junit.Test;

import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static org.junit.Assert.assertEquals;

public class NoContentImageTest extends ImageTest {

    @Override
    Image create(Identifier identifier) {
        return new NoContent(identifier, new Throwable());
    }

    @Test(expected = NoContent.NoContentException.class)
    public void thatRequestingContentFails() {
        create(anIdentifier().withDefaults().build()).getContent();
    }

    @Test
    public void thatGettingTheContentExposesTheRegisteredCause() {
        Throwable expected = new Throwable();
        try {
            new NoContent(anIdentifier().withDefaults().build(), expected).getContent();
        } catch (NoContent.NoContentException e) {
            assertEquals(expected, e.getCause());
        }
    }
}
