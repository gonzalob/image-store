package dridco.imagestore.model;

import dridco.imagestore.model.inmemory.InMemoryIdentifiers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.util.Date;

import static dridco.imagestore.model.ClientBuilder.aClient;
import static dridco.imagestore.model.Dates.parse;
import static dridco.imagestore.model.IdentifierBuilder.DEFAULT_ID;
import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;

@RunWith(MockitoJUnitRunner.class)
public class SecuredCommandTest {

	private Client owner = aClient().withDefaults().build();
	private Identifiers identifiers = new InMemoryIdentifiers();
	@Mock
	private Images images;
	@Mock
	private Command<Object> proxee;
	private Date now;
	private SecuredCommand<Object> tested;

	@Before
	public void setUp() throws ParseException {
		now = parse("2014-09-26 10:45:23");
		tested = new SecuredCommand<>(owner, DEFAULT_ID, proxee);
	}

	@Test
	public void thatItAllowsExecutionForTheIdentifiersOwner() throws ImageStoreException {
		identifiers.store(anIdentifier().withDefaults().ownedBy(owner).build());
		tested.execute(identifiers, images, now);
	}

	@Test(expected = DoesNotBelongToRequester.class)
	public void thatItRejectsExecutionForUsersOtherThanTheOwner() throws ImageStoreException {
		Client otherClient = aClient().withDefaults().withKey("other_client").build();
		identifiers.store(anIdentifier().withDefaults().ownedBy(otherClient).build());
		tested.execute(identifiers, images, now);
	}
}
