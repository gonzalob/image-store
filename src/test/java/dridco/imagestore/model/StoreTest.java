package dridco.imagestore.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.Date;

import static dridco.imagestore.model.ClientBuilder.aClient;
import static dridco.imagestore.model.Dates.parse;
import static dridco.imagestore.model.IdentifierBuilder.DEFAULT_ID;
import static dridco.imagestore.model.IdentifierBuilder.DEFAULT_PUBLIC_URL;
import static dridco.imagestore.model.ImageBuilder.DEFAULT_CONTENT;
import static dridco.imagestore.model.ImageBuilder.DEFAULT_CONTENT_TYPE;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StoreTest {

	@Mock
	private Identifiers identifiers;
	@Mock
	private Images images;
	@Mock
	private StorageEventListener listener;
	private Date now;
	private Store tested;

	@Before
	public void setUp() throws ParseException, MalformedURLException, UnsupportedContentType {
		now = parse("2014-09-26 10:39:11");
		tested = new CommandFactory(listener).store(aClient().withDefaults().build(),
				new StubCandidate(),
				DEFAULT_ID,
				DEFAULT_PUBLIC_URL);
	}

	@Test
	public void thatItCreatesAnIdentifierAndStoresTheImage() throws ImageStoreException {
		RawImage expected = tested.execute(identifiers, images, now);

		verify(identifiers).store(expected.getIdentifier());
		verify(images).store(expected);
	}

	@Test
	public void thatTheStorageTriggersAnEventWhenStoringAnImage() throws ImageStoreException {
		when(images.store(Mockito.<RawImage> any())).thenReturn(TRUE);

		tested.execute(identifiers, images, now);

		verify(listener).stored(Matchers.<Image> any());
	}

	@Test
	public void thatTheStorageTriggersAnEventWhenAliasingAnImage() throws ImageStoreException {
		tested.execute(identifiers, images, now);

		verify(listener).referenced(Matchers.<Image> any());
	}

	@Test
	public void thatTheStorageDoesntTriggerStoringEventWhenOnlyAliasingAnImage() throws ImageStoreException {
		when(images.store(Mockito.<RawImage> any())).thenReturn(FALSE);

		tested.execute(identifiers, images, now);

		verify(listener, never()).stored(Matchers.<Image> any());
	}
}
