package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilterFactory;
import org.junit.Test;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FailSafeFilterFactoryTest {

    @Test
    public void createsExceptionHandlerFilter() throws ImageFilterFactoryException {
        ImageFilterFactory filter = new FailSafeFilterFactory(new StubFilterFactory());
        assertEquals(
                filter.create(EMPTY).getClass(),
                OriginalContentOnException.class);
    }

    @Test
    public void createdFilterDelegatesOnInnerFilterFactory() throws ImageFilterFactoryException {
        String settings = "some random value";
        StubFilterFactory delegate = new StubFilterFactory(settings);
        ImageFilterFactory filter = new FailSafeFilterFactory(delegate);

        filter.create(settings);
        assertTrue(delegate.invoked());
    }
}
