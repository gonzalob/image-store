package dridco.imagestore.model.filters;

public class KeepHorizontalAspectRatioBottomResizeTest extends KeepHorizontalAspectRatioResizeTest {

	@Override
	protected Overflow verticalOverflow() {
		return new End();
	}

	@Override
	protected String position() {
		return "bottom";
	}
}
