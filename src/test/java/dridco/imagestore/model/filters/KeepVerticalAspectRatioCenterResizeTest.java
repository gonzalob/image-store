package dridco.imagestore.model.filters;

public class KeepVerticalAspectRatioCenterResizeTest extends KeepVerticalAspectRatioResizeTest {

	@Override
	protected Overflow horizontalOverflow() {
		return new Middle();
	}

	@Override
	protected String position() {
		return "center";
	}
}
