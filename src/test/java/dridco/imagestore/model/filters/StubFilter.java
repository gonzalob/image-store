package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;

import java.io.IOException;

public class StubFilter implements ImageFilter {

	private boolean invoked = false;

	@Override
	public byte[] filter(byte[] content) throws IOException {
		if(invoked) {
			throw new IllegalStateException("This filter should not be applied twice without calling clear");
		} else {
			invoked = true;
			return content;
		}
	}

	public Boolean isInvoked() {
		return invoked;
	}
	
	public void clear() {
		invoked = false;
	}

	@Override
	public String toString() {
		return "Invocation Tracking";
	}
}
