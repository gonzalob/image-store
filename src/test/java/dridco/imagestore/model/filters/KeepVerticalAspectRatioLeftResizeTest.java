package dridco.imagestore.model.filters;

public class KeepVerticalAspectRatioLeftResizeTest extends KeepVerticalAspectRatioResizeTest {

	@Override
	protected Overflow horizontalOverflow() {
		return new Start();
	}

	@Override
	protected String position() {
		return "left";
	}
}
