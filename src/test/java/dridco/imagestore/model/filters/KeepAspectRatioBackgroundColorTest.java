package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;
import dridco.imagestore.model.ImageFilterTest;

import static java.awt.Color.GREEN;
import static java.lang.String.format;

public class KeepAspectRatioBackgroundColorTest extends ImageFilterTest {

	@Override
	protected ImageFilter create() {
		return new Resize(new KeepAspectRatioOverflowing(
				new WidthAndHeight(SAMPLE_WIDTH, SAMPLE_HEIGHT * 2),
				new Middle(),
				new Middle(),
				GREEN));
	}

	@Override
	protected String expectedPngFilename() {
		return expectedFilename("png");
	}

	@Override
	protected String expectedGifFilename() {
		return expectedFilename("gif");
	}

	@Override
	protected String expectedJpegFilename() {
		return expectedFilename("jpg");
	}


	private String expectedFilename(String extension) {
		return format("keep-aspect-ratio-color.%s", extension);
	}
}
