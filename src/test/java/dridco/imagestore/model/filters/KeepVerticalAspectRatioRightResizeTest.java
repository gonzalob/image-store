package dridco.imagestore.model.filters;

public class KeepVerticalAspectRatioRightResizeTest extends KeepVerticalAspectRatioResizeTest {

	@Override
	protected Overflow horizontalOverflow() {
		return new End();
	}

	@Override
	protected String position() {
		return "right";
	}
}
