package dridco.imagestore.model.filters;

public class KeepHorizontalAspectRatioTopResizeTest extends KeepHorizontalAspectRatioResizeTest {

	@Override
	protected Overflow verticalOverflow() {
		return new Start();
	}

	@Override
	protected String position() {
		return "top";
	}
}
