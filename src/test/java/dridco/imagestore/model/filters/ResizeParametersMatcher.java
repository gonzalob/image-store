package dridco.imagestore.model.filters;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class ResizeParametersMatcher extends TypeSafeMatcher<Resize> {

	public static ResizeParametersMatcher hasWidthAndHeight(Integer width, Integer height) {
		return new ResizeParametersMatcher(width, height);
	}

	private Integer width;
	private Integer height;

	public ResizeParametersMatcher(Integer width, Integer height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("A resize filter with a width of ");
		description.appendValue(width);
		description.appendText(" and a height of ");
		description.appendValue(height);
	}

	@Override
	protected boolean matchesSafely(Resize item) {
		WidthAndHeight widthAndHeight = item.getScaling().getWidthAndHeight();
		return widthAndHeight.widthMatches(width) && widthAndHeight.heightMatches(height);
	}
}
