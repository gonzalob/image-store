package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;

import static java.awt.Color.BLACK;

/**
 * Tests for different horizontal overflow positions, keeping the height fixed
 */
public abstract class KeepVerticalAspectRatioResizeTest extends KeepAspectRatioResizeTest {

	protected abstract Overflow horizontalOverflow();

	@Override
	protected final ImageFilter create() {
		return new Resize(new KeepAspectRatioOverflowing(new WidthAndHeight(
				SAMPLE_WIDTH * 2, SAMPLE_HEIGHT),
				new Start(),
				horizontalOverflow(),
				BLACK));
	}
	
	@Override
	protected final String fixedAxis() {
		return "vertical";
	}
}
