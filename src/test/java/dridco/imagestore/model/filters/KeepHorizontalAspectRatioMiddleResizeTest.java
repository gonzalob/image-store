package dridco.imagestore.model.filters;

public class KeepHorizontalAspectRatioMiddleResizeTest extends KeepHorizontalAspectRatioResizeTest {

	@Override
	protected Overflow verticalOverflow() {
		return new Middle();
	}

	@Override
	protected String position() {
		return "middle";
	}
}
