package dridco.imagestore.model.filters;

import org.junit.Test;

import static dridco.imagestore.model.filters.ResizeParametersMatcher.hasWidthAndHeight;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ResizeFilterFactoryTest {

	@Test(expected = ImageFilterFactoryException.class)
	public void thatItFailsToBuildResizerWithoutHeightAndWidth() throws ImageFilterFactoryException {
		new ResizeFactory().create(EMPTY);
	}

	@Test
	public void thatItCreatesFilterWithOnlyWidthAndHeight() throws ImageFilterFactoryException {
		int width = 300;
		int height = 200;

		Resize filter = new ResizeFactory().create("" + width + "x" + height);
		assertThat(filter, hasWidthAndHeight(width, height));
	}

	@Test
	public void thatItDefaultsToRespectingTargetSizeOverAspectRatio() throws ImageFilterFactoryException {
		Resize filter = new ResizeFactory().create("45x30");
		assertTrue(filter.getScaling() instanceof AsRequested);
	}

	@Test
	public void thatItCreatesFilterWithOverflowScaling() throws ImageFilterFactoryException {
		Resize filter = new ResizeFactory().create("45x30.ccffffff");
		assertTrue(filter.getScaling() instanceof KeepAspectRatioOverflowing);
	}

	@Test
	public void thatItCreatesFilterWithComputedWidthScaling() throws ImageFilterFactoryException {
		Resize filter = new ResizeFactory().create("45x30.iw");
		assertTrue(filter.getScaling() instanceof KeepAspectRatioComputingIgnoredDimension);
	}

	@Test
	public void thatItCreatesFilterWithComputedHeightScaling() throws ImageFilterFactoryException {
		Resize filter = new ResizeFactory().create("45x30.ih");
		assertTrue(filter.getScaling() instanceof KeepAspectRatioComputingIgnoredDimension);
	}

	@Test(expected = ImageFilterFactoryException.class)
	public void thatItFailsToCreateScaledFilterWithNoIgnoredDimension() throws ImageFilterFactoryException {
		new ResizeFactory().create("45x30.i");
	}

	@Test(expected = ImageFilterFactoryException.class)
	public void thatItCreatesFilterWithInvalidIgnoredDimension() throws ImageFilterFactoryException {
		new ResizeFactory().create("45x30.ig");
	}

	@Test(expected = ImageFilterFactoryException.class)
	public void thatItCreatesFilterIgnoringBothDimensions() throws ImageFilterFactoryException {
		new ResizeFactory().create("45x30.iwh");
	}
}
