package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;

import static java.awt.Color.BLACK;

/**
 * Tests for different vertical overflow positions, keeping the width fixed
 */
public abstract class KeepHorizontalAspectRatioResizeTest extends KeepAspectRatioResizeTest {

	protected abstract Overflow verticalOverflow();

	@Override
	protected final ImageFilter create() {
		return new Resize(new KeepAspectRatioOverflowing(
				new WidthAndHeight(SAMPLE_WIDTH, SAMPLE_HEIGHT * 2),
				verticalOverflow(),
				new Start(),
				BLACK));
	}

	protected final String fixedAxis() {
		return "horizontal";
	}
}
