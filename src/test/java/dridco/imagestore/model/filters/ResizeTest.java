package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;
import dridco.imagestore.model.ImageFilterTest;

import static java.lang.String.format;
import static org.apache.commons.lang3.SystemUtils.IS_JAVA_1_7;
import static org.apache.commons.lang3.SystemUtils.IS_JAVA_1_8;

public class ResizeTest extends ImageFilterTest {

	private static final int RESIZED_HEIGHT = 50;
	private static final int RESIZED_WIDTH  = 50;

	@Override
	protected ImageFilter create() {
		return new Resize(new AsRequested(new WidthAndHeight(RESIZED_WIDTH, RESIZED_HEIGHT)));
	}

	@Override
	protected String expectedPngFilename() {
		return expectedFilename("png");
	}

	@Override
	protected String expectedGifFilename() {
		return expectedFilename("gif");
	}

	@Override
	protected String expectedJpegFilename() {
		return IS_JAVA_1_7 || IS_JAVA_1_8 ? jpegForJava7() : expectedFilename("jpg");
	}

	private String jpegForJava7() {
		return format("%sx%s-java7.jpg", RESIZED_HEIGHT, RESIZED_WIDTH);
	}

	private String expectedFilename(String extension) {
		return format("%sx%s.%s", RESIZED_HEIGHT, RESIZED_WIDTH, extension);
	}
}
