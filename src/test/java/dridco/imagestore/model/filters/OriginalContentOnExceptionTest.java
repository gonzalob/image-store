package dridco.imagestore.model.filters;

import dridco.imagestore.model.Identifier;
import dridco.imagestore.model.ImageFilter;
import org.junit.Test;

import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertArrayEquals;

public class OriginalContentOnExceptionTest {

    @Test
    public void fallsBackToOriginalContentIfFilterThrowsImageFilterException() throws IOException, ImageFilterException {
        ThrowImageFilterExceptionOnCall filter = new ThrowImageFilterExceptionOnCall();
        OriginalContentOnException tested = new OriginalContentOnException(filter);

        byte[] content = new byte[]{5, 2, 6, 7, 3, 2, 5, 3, 8, 2};
        assertArrayEquals(content, tested.filter(content));
    }

    @Test
    public void filteredImageIsReturnedWhenTheresNoFailure() throws IOException, ImageFilterException {
        FixedFilteredContent filter = new FixedFilteredContent();
        OriginalContentOnException tested = new OriginalContentOnException(filter);

        assertArrayEquals(FixedFilteredContent.FIXED_CONTENT, tested.filter(new byte[]{6, 2, 1}));
    }

    private static class ThrowImageFilterExceptionOnCall implements ImageFilter {

        @Override
        public byte[] filter(byte[] content) throws IOException, ImageFilterException {
            throw new ImageFilterException() {

                @Override
                protected String getMessageWithIdentifier(Identifier identifier) {
                    return EMPTY;
                }
            };
        }
    }

    private static class FixedFilteredContent implements ImageFilter {

        static final byte[] FIXED_CONTENT = new byte[]{5, 32, 5, 68, 3, 1, 45};

        @Override
        public byte[] filter(byte[] content) throws IOException, ImageFilterException {
            return FIXED_CONTENT;
        }
    }
}
