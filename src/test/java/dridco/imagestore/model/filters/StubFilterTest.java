package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;
import dridco.imagestore.model.ImageFilterTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StubFilterTest extends ImageFilterTest {

	private static final byte[] ANY_IMAGE = new byte[0];
	private StubFilter filter = new StubFilter();

	@Before
	public void checkInitialState() {
		assertFalse(filter.isInvoked());
	}

	@After
	public void assertInvocation() {
		assertTrue(filter.isInvoked());
	}

	@Override
	protected ImageFilter create() {
		return filter;
	}

	@Override
	protected String expectedPngFilename() {
		return ORIGINAL_PNG;
	}

	@Override
	protected String expectedGifFilename() {
		return ORIGINAL_GIF;
	}

	@Override
	protected String expectedJpegFilename() {
		return ORIGINAL_JPG;
	}

	@Test(expected = IllegalStateException.class)
	public void thatItCannotBeCalledTwice() throws IOException {
		filter.filter(ANY_IMAGE);
		filter.filter(ANY_IMAGE);
	}

	@Test
	public void thatItResetsInvocationStateWhenCleared() throws IOException {
		filter.filter(ANY_IMAGE);
		filter.clear();
		assertFalse(filter.isInvoked());
		
		// filter again, so the @After check will find it invoked and not fail
		filter.filter(ANY_IMAGE);
	}
}
