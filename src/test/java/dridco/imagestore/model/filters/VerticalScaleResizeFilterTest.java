package dridco.imagestore.model.filters;

public class VerticalScaleResizeFilterTest extends ScaledResizeFilterTest {

	@Override
	protected IgnoredDimension ignoredDimension() {
		return IgnoredDimension.WIDTH;
	}

	@Override
	protected Integer widthMultiplier() {
		return 1;
	}

	@Override
	protected Integer heightMultiplier() {
		return 2;
	}
}
