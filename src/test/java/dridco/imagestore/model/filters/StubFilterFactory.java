package dridco.imagestore.model.filters;

import com.google.common.base.Optional;
import dridco.imagestore.model.ImageFilter;
import dridco.imagestore.model.ImageFilterFactory;

import static com.google.common.base.Optional.absent;
import static org.junit.Assert.assertEquals;

public class StubFilterFactory implements ImageFilterFactory {

	private Optional<String> expectedConfig;
	private boolean invoked = false;

	public StubFilterFactory() {
		expectedConfig = absent();
	}

	public StubFilterFactory(String expectedConfig) {
		this.expectedConfig = Optional.of(expectedConfig);
	}

	@Override
	public ImageFilter create(String config) {
		assertEquals(expectedConfig.or(config), config);
		invoked = true;
		return new StubFilter();
	}

	public boolean invoked() {
		return invoked;
	}
}
