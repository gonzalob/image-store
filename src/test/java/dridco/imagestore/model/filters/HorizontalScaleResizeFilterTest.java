package dridco.imagestore.model.filters;

public class HorizontalScaleResizeFilterTest extends ScaledResizeFilterTest {

	@Override
	protected IgnoredDimension ignoredDimension() {
		return IgnoredDimension.HEIGHT;
	}

	@Override
	protected Integer widthMultiplier() {
		return 2;
	}

	@Override
	protected Integer heightMultiplier() {
		return 1;
	}
}
