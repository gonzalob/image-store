package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilter;
import dridco.imagestore.model.ImageFilterTest;

public abstract class ScaledResizeFilterTest extends ImageFilterTest {

	protected abstract Integer widthMultiplier();
	protected abstract Integer heightMultiplier();

	protected abstract IgnoredDimension ignoredDimension();

	@Override
	protected ImageFilter create() {
		return new Resize(new KeepAspectRatioComputingIgnoredDimension(
				new WidthAndHeight(SAMPLE_WIDTH * widthMultiplier(), SAMPLE_HEIGHT * heightMultiplier()), ignoredDimension()));
	}

	@Override
	protected String expectedPngFilename() {
		return expectedName("png");
	}

	@Override
	protected String expectedGifFilename() {
		return expectedName("gif");
	}

	@Override
	protected String expectedJpegFilename() {
		return expectedName("jpg");
	}

	private String expectedName(String extension) {
		return "200x200." + extension;
	}
}
