package dridco.imagestore.model.filters;

import dridco.imagestore.model.ImageFilterTest;

import static java.lang.String.format;

public abstract class KeepAspectRatioResizeTest extends ImageFilterTest {

	protected abstract String position();
	protected abstract String fixedAxis();

	@Override
	protected final String expectedPngFilename() {
		return expectedFilename("png");
	}

	@Override
	protected final String expectedGifFilename() {
		return expectedFilename("gif");
	}

	@Override
	protected final String expectedJpegFilename() {
		return expectedFilename("jpg");
	}

	private String expectedFilename(String extension) {
		return format("keep-%s-aspect-ratio-%s.%s",
				fixedAxis(),
				position(),
				extension);
	}
}
