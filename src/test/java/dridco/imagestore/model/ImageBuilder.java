package dridco.imagestore.model;

import org.springframework.http.MediaType;

import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static org.springframework.http.MediaType.IMAGE_JPEG;

public final class ImageBuilder {

	public static final String DEFAULT_ID = "logo";
	public static final String DEFAULT_HASH = "0405557b301a1b689df0f02566bec761d7783232";
	public static final Identifier DEFAULT_IDENTIFIER = anIdentifier().withDefaults()
			.identifiedBy(DEFAULT_ID)
			.withHash(DEFAULT_HASH).build();
	public static final MediaType DEFAULT_CONTENT_TYPE = IMAGE_JPEG;
	public static final byte[] DEFAULT_CONTENT = new byte[]{
            -1, -40, -1, -32, 0, 16, 74, 70, 73, 70, 0, 1, 1, 1, 0, 96, 0, 96, 0,
            0, -1, -37, 0, 67, 0, 8, 6, 6, 7, 6, 5, 8, 7, 7, 7, 9, 9, 8, 10, 12,
            20, 13, 12, 11, 11, 12, 25, 18, 19, 15, 20, 29, 26, 31, 30, 29, 26,
            28, 28, 32, 36, 46, 39, 32, 34, 44, 35, 28, 28, 40, 55, 41, 44, 48,
            49, 52, 52, 52, 31, 39, 57, 61, 56, 50, 60, 46, 51, 52, 50, -1, -37,
            0, 67, 1, 9, 9, 9, 12, 11, 12, 24, 13, 13, 24, 50, 33, 28, 33, 50,
            50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,
            50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,
            50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, -1, -64,
            0, 17, 8, 0, 1, 0, 1, 3, 1, 34, 0, 2, 17, 1, 3, 17, 1, -1, -60, 0, 21,
            0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, -1, -60, 0,
            20, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -60,
            0, 20, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -60,
            0, 20, 17, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -38,
            0, 12, 3, 1, 0, 2, 17, 3, 17, 0, 63, 0, -65, -128, 15, -1, -39};

	private Identifier id;
	private byte[] content;

	public static ImageBuilder anImage() {
		return new ImageBuilder();
	}

	public ImageBuilder withDefaults() {
		id = DEFAULT_IDENTIFIER;
		content = DEFAULT_CONTENT;
		return this;
	}

	public ImageBuilder identifiedBy(Identifier id) {
		this.id = id;
		return this;
	}

	public ImageBuilder withContent(byte[] content) {
		this.content = content;
		return this;
	}

	public RawImage build() {
		return new RawImage(id, content);
	}

	private ImageBuilder() {}
}
