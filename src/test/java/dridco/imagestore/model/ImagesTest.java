package dridco.imagestore.model;

import org.junit.Before;
import org.junit.Test;

import static dridco.imagestore.model.IdentifierBuilder.DEFAULT_HASH;
import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static dridco.imagestore.model.ImageBuilder.anImage;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class ImagesTest {

	protected abstract Images create() throws Exception;

	private static final byte[] SAMPLE_IMAGE = new byte[] { 76, 126, 32, 77, 123 };
	private Images tested;

	@Before
	public void createTestedObject() throws Exception {
		tested = create();
	}

	@Test
	public void thatHasNoInitialElements() {
		assertTrue(tested.count() == 0);
	}

	@Test
	public void thatItAcceptsTwoIdentifiersForTheSameImage() {
		doStore("first");
		doStore("second");

		assertTrue(tested.count() == 1);
	}

	@Test
	public void thatItReturnsTrueWhenActuallyStoringAnImage() {
		assertTrue(doStore("third"));
	}

	@Test
	public void thatItReturnsFalseWhenNotStoringAnImage() {
		String id = "fourth";
		doStore(id);
		assertFalse(doStore(id));
	}

	@Test
	public void thatWhenStoringTwoDifferentHashesHoldsTwoImages() {
		tested.store(new RawImage(anIdentifier().withDefaults().withHash("fifth").build(), SAMPLE_IMAGE));
		tested.store(new RawImage(anIdentifier().withDefaults().withHash("sixth").build(), SAMPLE_IMAGE));

		assertTrue(tested.count() == 2);
	}

	@Test
	public void thatAnImageCanBeRemoved() {
		String hash = "seventh";
		doStore(hash);
		tested.remove(DEFAULT_HASH);

		assertTrue(tested.count() == 0);
	}

	@Test
	public void thatTheReturnedImageHasItsReferencingIdentifier() {
		String first = "eighth";
		String second = "nineth";
		doStore(first);
		doStore(second);
		
		assertIdentifierMatches(first);
		assertIdentifierMatches(second);
	}

	@Test
	public void thatRetrievingInexistingImageLoadsOneWithoutContent() {
		tested.get(anIdentifier().withDefaults().build());
	}

	private void assertIdentifierMatches(String identifier) {
		Image image = tested.get(anIdentifier().withDefaults().identifiedBy(identifier).build());
		assertTrue(image.getIdentifier().identifiedBy(identifier));
	}

	private boolean doStore(String id) {
		Identifier identifier = anIdentifier().withDefaults().identifiedBy(id).build();
		RawImage image = anImage().withDefaults().identifiedBy(identifier).build();
		return tested.store(image);
	}
}
