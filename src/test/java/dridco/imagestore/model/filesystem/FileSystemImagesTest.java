package dridco.imagestore.model.filesystem;

import dridco.imagestore.model.Images;
import dridco.imagestore.model.ImagesTest;
import org.junit.After;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.Files.*;

public class FileSystemImagesTest extends ImagesTest {

	private Path temp;

	@After
	public void removeTemporaryStorage() throws IOException {
		walkFileTree(temp, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				delete(file);
				return CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				delete(dir);
				return CONTINUE;
			}
		});
	}

	@Override
	protected Images create() throws IOException {
		temp = createTempDirectory(FileSystemImages.class.getSimpleName());
		return new FileSystemImages(temp);
	}
}
