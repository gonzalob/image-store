package dridco.imagestore.model;

import com.google.common.collect.Iterables;
import liquibase.exception.LiquibaseException;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

import static dridco.imagestore.model.ClientBuilder.aClient;
import static dridco.imagestore.model.Dates.parse;
import static dridco.imagestore.model.IdentifierBuilder.*;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.*;

public abstract class IdentifiersTest {

	public abstract Identifiers create() throws SQLException, LiquibaseException;

	protected Client OWNER = aClient().withDefaults().withInitialValidity(5L).withValidityInterval(1440L).build();
	private Identifiers tested;

	@Before
	public final void createTestedObject() throws Exception {
		tested = create();
	}

	@Test
	public void thatItDoesNotContainAnUnknownIdentifier() throws IdentifiersException {
		assertFalse(tested.contains("an unknown identifier"));
	}

	@Test
	public void thatContainsAnIdentifierAfterStoringIt() throws IdentifiersException {
		String id = "something";
		tested.store(anIdentifier().withDefaults().ownedBy(OWNER).identifiedBy(id).build());
		assertTrue(tested.contains(id));
	}

	@Test(expected = IdentifierDoesNotExist.class)
	public void thatRequestingSomethingThatDoesNotExistFails() throws IdentifiersException {
		tested.get("non existent identifier");
	}

	@Test
	public void thatCanReturnStoredIdentifier() throws IdentifiersException {
		String id = "known";
		Identifier expected = anIdentifier().withDefaults().ownedBy(OWNER).identifiedBy(id).build();
		tested.store(expected);
		assertEquals(expected, tested.get(id));
	}

	@Test(expected = IdentifierExists.class)
	public void thatRefusesToStoreTheSameIdentifierTwice() throws IdentifiersException {
		Identifier identifier = anIdentifier().withDefaults().ownedBy(OWNER).build();
		tested.store(identifier);
		tested.store(identifier);
	}

	@Test
	public void thatRemovingStillLinkedHashDoesntTriggerDeletion() throws IdentifiersException {
		StubImages images = new StubImages();

		String hash = "hash";
		String first = "1";
		tested.store(anIdentifier().withDefaults().ownedBy(OWNER).identifiedBy(first).withHash(hash).build());
		String second = "2";
		tested.store(anIdentifier().withDefaults().ownedBy(OWNER).identifiedBy(second).withHash(hash).build());

		tested.remove(tested.get(first)).remove(images);

		assertFalse(images.removeInvoked());
	}

	@Test
	public void thatRemovingAnOrphanedHashTriggersItsDeletion() throws IdentifiersException {
		StubImages images = new StubImages();

		String id = "1";
		tested.store(anIdentifier().withDefaults().ownedBy(OWNER).identifiedBy(id).build());

		tested.remove(tested.get(id)).remove(images);

		assertTrue(images.removeInvoked());
	}

	@Test(expected = IdentifierDoesNotExist.class)
	public void thatRemovingAnInexistingIdentifierFailsWithException() throws IdentifiersException {
		tested.remove(anIdentifier().withDefaults().build());
	}

	@Test
	public void thatItPreservesTheOwner() throws IdentifiersException {
		Identifier identifier = anIdentifier().withDefaults().ownedBy(OWNER).build();
		tested.store(identifier);
		assertTrue(tested.get(DEFAULT_ID).isOwnedBy(OWNER));
	}

	@Test
	public void thatItFiltersCandidatesWithInitialValidityExpired() throws IdentifiersException, ParseException {
		Identifier candidate =
				anIdentifier().withDefaults().identifiedBy("in").ownedBy(OWNER).createdOn(parse("2014-09-30 14:22:12")).build();
		tested.store(candidate);
		tested.store(
				anIdentifier().withDefaults().identifiedBy("out").ownedBy(OWNER).createdOn(parse("2014-09-30 14:26:37")).build());

		assertThat(tested.candidatesToRelease(new CleanupRequest(OWNER, parse("2014-09-30 14:29:39"))), contains(candidate));
	}

	@Test
	public void thatItFiltersCandidatesToBeRemovedWhenRenewedValidityExpired() throws IdentifiersException, ParseException {
		Identifier candidate =
				anIdentifier().withDefaults().identifiedBy( "in").ownedBy(OWNER).validatedOn(parse("2014-09-26 10:23:24")).build();
		tested.store(candidate);
		tested.store(
				anIdentifier().withDefaults().identifiedBy("out").ownedBy(OWNER).validatedOn(parse("2014-09-27 5:37:48")).build());

		assertThat(tested.candidatesToRelease(new CleanupRequest(OWNER, parse("2014-09-27 11:41:21"))), contains(candidate));
	}

	@Test
	public void thatItConsidersOnlyTheRequestedClientWhenFilteringCandidates() throws IdentifiersException, ParseException {
		String sameTime = "2014-09-26 15:51:11";
		Client other = aClient().withDefaults().withKey("other").build();
		Identifier candidate =
				anIdentifier().withDefaults().identifiedBy( "in").ownedBy(OWNER).createdOn(parse(sameTime)).build();
		tested.store(candidate);
		tested.store(
				anIdentifier().withDefaults().identifiedBy("out").ownedBy(other).createdOn(parse(sameTime)).build());

		assertThat(tested.candidatesToRelease(new CleanupRequest(OWNER, parse("2014-09-28 16:13:42"))), contains(candidate));
	}

	@Test
	public void thatItKeepsTheCorrectLastCheckedDate() throws IdentifiersException {
		tested.store(anIdentifier().withDefaults().build());
		assertEquals(DEFAULT_CREATION_DATE, tested.get(DEFAULT_ID).getLastChecked());
	}

	@Test
	public void thatChangesToAnIdentifierAreRecognizedForNewlyRetrievedInstances() throws IdentifiersException, ParseException {
		String id = "refreshed";
		Date created = parse("2014-09-28 17:41:35");
		Date validated = parse("2014-09-29 10:47:52");

		tested.store(anIdentifier().withDefaults().identifiedBy(id).createdOn(created).build());
		Identifier retrieved = tested.get(id);

		retrieved.validatedOn(validated);
		assertEquals(validated, tested.get(id).getLastChecked());
	}

	@Test(expected = IdentifierDeleted.class)
	public void thatItFailsToLoadDeletedImage() throws IdentifiersException {
		Identifier identifier = anIdentifier().withDefaults().build();
		tested.store(identifier);
		tested.remove(identifier);
		tested.get(DEFAULT_ID);
	}

    @Test(expected = IdentifierDeleted.class)
    public void thatItLocksDeletedIdentifiers() throws IdentifiersException {
        Identifier identifier = anIdentifier().withDefaults().build();
        tested.store(identifier);
        tested.remove(identifier);
        tested.store(identifier);
    }

    @Test(expected = IdentifierDoesNotExist.class)
    public void thatDeletedImagesCanBePurged() throws IdentifiersException {
        Identifier identifier = anIdentifier().withDefaults().build();
        tested.store(identifier);
        tested.remove(identifier);
        tested.purge();
        tested.get(DEFAULT_ID);
    }

    @Test
	public void thatIdentifiersAreNotAvailableForReleaseWhileInProcess() throws IdentifiersException, ParseException {
        Identifier candidate =
                anIdentifier().withDefaults().identifiedBy("any").ownedBy(OWNER).createdOn(parse("2017-02-07 16:52:12")).build();
        tested.store(candidate);

        tested.candidatesToRelease(new CleanupRequest(OWNER, parse("2017-03-07 14:29:39")));
        assertThat(tested.candidatesToRelease(new CleanupRequest(OWNER, parse("2017-03-07 14:29:42"))), Matchers.<Identifier>emptyIterable());
    }

    @Test
	public void thatIdentifiersAreNotAvailableForReleaseWhilwweInProcess() throws IdentifiersException, ParseException {
        Identifier candidate =
                anIdentifier().withDefaults().identifiedBy("released").ownedBy(OWNER).createdOn(parse("2017-02-07 16:52:12")).build();
        tested.store(candidate);

        CleanupRequest request = new CleanupRequest(OWNER, parse("2017-03-07 14:29:39"));
        Iterable<Identifier> releasable = tested.candidatesToRelease(request);
        tested.completed(request, releasable);
        assertThat(tested.candidatesToRelease(new CleanupRequest(OWNER, parse("2017-03-07 14:29:42"))), contains(candidate));
    }
}
