package dridco.imagestore.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static dridco.imagestore.model.ImageBuilder.anImage;
import static java.util.Arrays.asList;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DemultiplexerStorageEventListenerTest {

	@Mock
	private StorageEventListener first;
	@Mock
	private StorageEventListener second;
	private DemultiplexerStorageEventListener tested;

	@Before
	public void setUp() {
		tested = new DemultiplexerStorageEventListener(asList(first, second));
	}

	@Test
	public void thatItPropagatesLoadedEventsToAllParticipants() {
		Image image = anImage().withDefaults().build();
		tested.loaded(image);
		
		verify(first).loaded(image);
		verify(second).loaded(image);
	}

	@Test
	public void thatItPropagatesReferencedEventsToAllParticipants() {
		Image image = anImage().withDefaults().build();
		tested.referenced(image);
		
		verify(first).referenced(image);
		verify(second).referenced(image);
	}

	@Test
	public void thatItPropagatesStoredEventsToAllParticipants() {
		Image image = anImage().withDefaults().build();
		tested.stored(image);
		
		verify(first).stored(image);
		verify(second).stored(image);
	}

	@Test
	public void thatItPropagatesReleasedEventsToAllParticipants() {
		Identifier identifier = anIdentifier().withDefaults().build();
		tested.released(identifier);
		
		verify(first).released(identifier);
		verify(second).released(identifier);
	}

	@Test
	public void thatItPropagatesRemovedEventsToAllParticipants() {
		Image image = anImage().withDefaults().build();
		tested.removed(image);
		
		verify(first).removed(image);
		verify(second).removed(image);
	}
}
