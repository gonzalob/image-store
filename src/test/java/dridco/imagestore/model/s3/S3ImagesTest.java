package dridco.imagestore.model.s3;

import dridco.imagestore.model.Images;
import dridco.imagestore.model.ImagesTest;

import static com.amazonaws.regions.Regions.SA_EAST_1;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.Assume.assumeTrue;

/**
 * This class requires credentials to access Amazon S3, and an empty bucket to
 * work with.
 * <p>
 * Provide these on runtime, by setting the {@link #S3_API_KEY_PROPERTY},
 * {@link #S3_SECRET_PROPERTY} and {@link #S3_BUCKET_PROPERTY} system properties
 */
public class S3ImagesTest extends ImagesTest {

    private static final String S3_API_KEY_PROPERTY = "test.s3.api-key";
    private static final String S3_SECRET_PROPERTY = "test.s3.secret";
    private static final String S3_BUCKET_PROPERTY = "test.s3.bucket";

    @Override
    protected Images create() {
        String apiKey = System.getProperty(S3_API_KEY_PROPERTY);
        String secret = System.getProperty(S3_SECRET_PROPERTY);
        String bucket = System.getProperty(S3_BUCKET_PROPERTY);

        assumeTrue(
                "Test disabled, missing configuration. See this class' javadoc for details",
                isNotBlank(apiKey) && isNotBlank(secret) && isNotBlank(bucket));

        S3Images tested = new S3Images(apiKey, secret, SA_EAST_1, bucket);
        tested.clear();
        return tested;
    }
}
