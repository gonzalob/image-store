package dridco.imagestore.model;

import dridco.imagestore.controllers.Constants;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.google.common.net.HttpHeaders.AUTHORIZATION;
import static dridco.imagestore.controllers.Constants.CLIENT_KEY_HEADER;
import static dridco.imagestore.model.ImageBuilder.DEFAULT_CONTENT_TYPE;
import static dridco.imagestore.model.ImageBuilder.DEFAULT_ID;
import static dridco.imagestore.model.ImageBuilder.anImage;
import static org.apache.http.HttpHeaders.IF_MATCH;
import static org.springframework.http.HttpMethod.HEAD;
import static org.springframework.http.MediaType.IMAGE_GIF;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;

public final class ImageRequests {

	public static Image image() {
		return anImage().withDefaults().build();
	}

	public static String path() {
		return Constants.ROOT_PATH + DEFAULT_ID;
	}

	public static MediaType mediaType() {
		return DEFAULT_CONTENT_TYPE;
	}

	public static MockHttpServletRequestBuilder get() {
		return MockMvcRequestBuilders.get(path()).accept(mediaType());
	}

	public static MockHttpServletRequestBuilder head() {
		return request(HEAD, path()).accept(mediaType());
	}

	public static MockHttpServletRequestBuilder post() {
		return identified(postWithoutIdentification());
	}

	public static MockHttpServletRequestBuilder postWithoutIdentification() {
		return completePutOrPost(MockMvcRequestBuilders.post(Constants.ROOT_PATH));
	}

	public static MockHttpServletRequestBuilder put() {
		return secured(putWithoutIdentification());
	}

	public static MockHttpServletRequestBuilder delete() {
		return secured(deleteWithoutIdentification());
	}

	public static MockHttpServletRequestBuilder putWithoutIdentification() {
		return completePutOrPost(MockMvcRequestBuilders.put(path()));
	}

	public static MockHttpServletRequestBuilder deleteWithoutIdentification() {
		return MockMvcRequestBuilders.delete(path()).header(IF_MATCH, "\"" + ImageBuilder.DEFAULT_HASH + "\"");
	}

	private static MockHttpServletRequestBuilder completePutOrPost(MockHttpServletRequestBuilder base) {
		return base.contentType(mediaType()).content(ImageBuilder.DEFAULT_CONTENT);
	}

	public static MockHttpServletRequestBuilder secured(MockHttpServletRequestBuilder postWithoutIdentification) {
		return postWithoutIdentification.header(AUTHORIZATION, "Basic YWRtaW46czNjcjN0");
	}

	private static MockHttpServletRequestBuilder identified(MockHttpServletRequestBuilder postWithoutIdentification) {
		return postWithoutIdentification.header(CLIENT_KEY_HEADER, "admin");
	}

	private ImageRequests() {
	}
}
