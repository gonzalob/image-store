package dridco.imagestore.model;

import org.junit.Test;

import java.net.MalformedURLException;

import static org.junit.Assert.assertEquals;

public class URLTemplateTest {

	@Test
	public void thatItBuildsUrlsFromTemplate() throws MalformedURLException {
		String template = "http://localhost/{id}";
		URLTemplate tested = new URLTemplate(template);

		assertEquals("http://localhost/identifier", tested.urlFor("identifier").toString());
	}
}
