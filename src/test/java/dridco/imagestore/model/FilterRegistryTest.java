package dridco.imagestore.model;

import dridco.imagestore.model.filters.ImageFilterFactoryException;
import dridco.imagestore.model.filters.StubFilter;
import dridco.imagestore.model.filters.StubFilterFactory;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

public class FilterRegistryTest {

	private static final String UNDEFINED_FILTER_KEY = "u";
	private static final String STUB_FILTER_KEY_1 = "sf1";
	private static final String STUB_FILTER_KEY_2 = "sf2";
	
	private static final String SPLITTER = "_";
	private static final String EXPECTED_CONFIG = "nothing";
	private static final String STUB_FILTER_WITH_CONFIGURATION = STUB_FILTER_KEY_2 + SPLITTER + EXPECTED_CONFIG;

	private FilterRegistry tested = new FilterRegistryBuilder().withNameConfigSplitter(SPLITTER)
		.addFilter(STUB_FILTER_KEY_1, new StubFilterFactory())
		.addFilter(STUB_FILTER_KEY_2, new StubFilterFactory(EXPECTED_CONFIG)).build();

	@Test
	public void thatItIgnoresUnknownFilters() throws ImageFilterFactoryException {
		assertThat(tested.parse(singletonList(UNDEFINED_FILTER_KEY)).getFilters(), empty());
	}
	
	@Test
	public void thatItAppliesTheSelectedFilter() throws ImageFilterFactoryException {
		List<ImageFilter> filters = tested.parse(singletonList(STUB_FILTER_KEY_1)).getFilters();
		assertThat(filters, Matchers.<ImageFilter> hasItem(instanceOf(StubFilter.class)));
	}
	
	@Test
	public void thatItAppliesSelectedFiltersAndIgnoresUnknownOnes() throws ImageFilterFactoryException {
		List<ImageFilter> filters = tested.parse(asList(UNDEFINED_FILTER_KEY, STUB_FILTER_WITH_CONFIGURATION)).getFilters();
		assertThat(filters, Matchers.<ImageFilter> hasItem(instanceOf(StubFilter.class)));
	}
}
