package dridco.imagestore.model;

import dridco.imagestore.model.inmemory.InMemoryIdentifiers;
import dridco.imagestore.model.inmemory.InMemoryImages;
import dridco.imagestore.model.jdbc.StubClients;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;

import static dridco.imagestore.model.ClientBuilder.DEFAULT_IDENTIFIER_VALIDATION_URL;
import static dridco.imagestore.model.ClientBuilder.aClient;
import static dridco.imagestore.model.IdentifierBuilder.DEFAULT_ID;
import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static dridco.imagestore.model.ImageBuilder.anImage;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

public class GarbageCollectorTest {

	private GarbageCollector tested;

	@Test
	public void thatItDoesNothingWhenThereAreNoClientsDefined() throws ParseException, IdentifiersException {
		Identifiers identifiers = mock(Identifiers.class);
		IdentifierValidator validator = mock(IdentifierValidator.class);
		tested = new GarbageCollector(new StubClients(), identifiers, new InMemoryImages(), validator);

		tested.execute(Dates.parse("2014-09-29 11:18:31"));

		verifyZeroInteractions(identifiers, validator);
	}

	@Test
	public void thatItRequestsIdentifiersToValidateForEachKnownClient() throws ParseException, IdentifiersException {
		Identifiers identifiers = mock(Identifiers.class);
		IdentifierValidator validator = mock(IdentifierValidator.class);
		when(identifiers.candidatesToRelease(Mockito.<CleanupRequest> any())).thenReturn(Collections.<Identifier> emptyList());

		Client first = aClient().withDefaults().withKey("first").build();
		Client second = aClient().withDefaults().withKey("second").build();
		tested = new GarbageCollector(new StubClients(first, second), identifiers, new InMemoryImages(), validator);

		Date when = Dates.parse("2014-09-29 11:46:27");
		tested.execute(when);

		verify(identifiers).candidatesToRelease(new CleanupRequest(first, when));
		verify(identifiers).candidatesToRelease(new CleanupRequest(second, when));

		verifyZeroInteractions(validator);
	}

	@Test
	public void thatItRevalidatesAnIdentifierWhenItIsStillInUse() throws ParseException, IdentifiersException {
		Identifiers identifiers = new InMemoryIdentifiers();
		IdentifierValidator validator = mock(IdentifierValidator.class);
		Identifier stillInUse = anIdentifier().withDefaults().createdOn(Dates.parse("2014-09-28 23:47:19")).build();
		identifiers.store(stillInUse);
		when(validator.stillInUse(DEFAULT_IDENTIFIER_VALIDATION_URL, stillInUse)).thenReturn(TRUE);

		Client client = aClient().withDefaults().withValidityInterval(10L).build();
		tested = new GarbageCollector(new StubClients(client), identifiers, new InMemoryImages(), validator);

		Date when = Dates.parse("2014-09-29 11:46:27");
		tested.execute(when);

		assertEquals(when, stillInUse.getLastChecked());
	}

	@Test
	public void thatItReleasesAnIdentifierWhenItIsNoLongerUsed() throws ParseException, IdentifiersException {
		Identifiers identifiers = new InMemoryIdentifiers();
		IdentifierValidator validator = mock(IdentifierValidator.class);
		Identifier released = anIdentifier().withDefaults().createdOn(Dates.parse("2014-09-28 23:47:19")).build();
		identifiers.store(released);
		when(validator.stillInUse(DEFAULT_IDENTIFIER_VALIDATION_URL, released)).thenReturn(FALSE);
		Images images = new InMemoryImages();
		images.store(anImage().withDefaults().identifiedBy(released).build());

		Client client = aClient().withDefaults().withValidityInterval(10L).build();
		tested = new GarbageCollector(new StubClients(client), identifiers, images, validator);

		Date when = Dates.parse("2014-09-29 11:46:27");
		tested.execute(when);

		assertFalse(identifiers.contains(DEFAULT_ID));
	}
}
