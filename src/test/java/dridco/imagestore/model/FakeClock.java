package dridco.imagestore.model;

import java.text.ParseException;
import java.util.Date;

import static dridco.imagestore.model.Dates.parse;

public class FakeClock implements Clock {

	public static FakeClock on(String date) throws ParseException {
		return new FakeClock(parse(date));
	}

	private Date when;

	public FakeClock(Date when) {
		this.when = when;
	}

	@Override
	public Date now() {
		return when;
	}
}
