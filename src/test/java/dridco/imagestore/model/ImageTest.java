package dridco.imagestore.model;

import org.junit.Test;

import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;

public abstract class ImageTest {

	abstract Image create(Identifier identifier);

	@Test(expected = NullPointerException.class)
	public void thatAnImageCannotBeBuiltWithoutAnIdentifier() {
		create(null);
	}
}
