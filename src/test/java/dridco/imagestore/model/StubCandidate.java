package dridco.imagestore.model;

import org.springframework.http.MediaType;

import static org.springframework.http.MediaType.IMAGE_JPEG;

public class StubCandidate implements Candidate {

    @Override
    public MediaType getType() {
        return IMAGE_JPEG;
    }

    @Override
    public byte[] getContent() {
        return new byte[0];
    }
}
