package dridco.imagestore.model;

import org.junit.Test;

import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static dridco.imagestore.model.ImageBuilder.DEFAULT_CONTENT;

public class RawImageTest extends ImageTest {

    @Override
    Image create(Identifier identifier) {
        return create(identifier, DEFAULT_CONTENT);
    }

    @Test(expected = NullPointerException.class)
    public void thatAnImageCannotBeBuiltWithoutItsContent() {
        create(anIdentifier().withDefaults().build(), null);
    }

    private RawImage create(Identifier identifier, byte[] content) {
        return new RawImage(identifier, content);
    }
}
