package dridco.imagestore.model;

import java.net.MalformedURLException;
import java.net.URL;

public class ClientBuilder {

	public static final String DEFAULT_KEY = "test_client_key";
	public static final URL DEFAULT_IDENTIFIER_VALIDATION_URL;
	public static final Long DEFAULT_VALIDITY_INTERVAL = 1440L;
	public static final Long DEFAULT_INITIAL_VALIDITY = 5L;
	public static final URLFactory DEFAULT_URL_FACTORY = new AsRequested();

	static {
		try {
			DEFAULT_IDENTIFIER_VALIDATION_URL = new URL("http://localhost/still_in_use");
		} catch (MalformedURLException e) {
			throw new AssertionError("MalformedURLException with a fixed String representing a valid URL");
		}
	}

	public static ClientBuilder aClient() {
		return new ClientBuilder();
	}

	private String key;
	private Long validityInterval;
	private Long initialValidity;
	private URL callback;
	private URLFactory urlFactory;

	public ClientBuilder withDefaults() {
		key = DEFAULT_KEY;
		validityInterval = DEFAULT_VALIDITY_INTERVAL;
		initialValidity = DEFAULT_INITIAL_VALIDITY;
		callback = DEFAULT_IDENTIFIER_VALIDATION_URL;
		urlFactory = DEFAULT_URL_FACTORY;
		return this;
	}

	public ClientBuilder withValidityInterval(Long minutes) {
		validityInterval = minutes;
		return this;
	}

	public ClientBuilder withInitialValidity(Long minutes) {
		this.initialValidity = minutes;
		return this;
	}

	public ClientBuilder withKey(String key) {
		this.key = key;
		return this;
	}

	public Client build() {
		return new Client(key, initialValidity, validityInterval, callback, urlFactory);
	}
}
