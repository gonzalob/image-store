package dridco.imagestore.model;

import dridco.imagestore.model.inmemory.InMemoryIdentifiers;
import dridco.imagestore.model.inmemory.InMemoryImages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;

import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class StorageTest {

	private Identifiers identifiers = new InMemoryIdentifiers();
	private Images images = new InMemoryImages();
	private FakeClock clock;
	private Storage tested;

	@Before
	public void setUp() throws ParseException {
		clock = FakeClock.on("2014-09-26 10:31:25");
		tested = new Storage(identifiers, images, clock);
	}

	@Test
	public void thatItTriggersTheCommandWithItsIdentifiersAndImages() throws ImageStoreException {
		Command<?> command = mock(Command.class);

		tested.execute(command);

		Mockito.verify(command).execute(identifiers, images, clock.now());
	}
}
