package dridco.imagestore.model;

import org.junit.Test;
import org.springframework.http.MediaType;

import static dridco.imagestore.model.MagicNumberCandidate.SUPPORTED_TYPES;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.MediaType.*;

public class MagicNumberCandidateTest {

    @Test(expected = UnsupportedContentType.class)
    public void failsWhenContentTypeIsNotSupported() throws UnsupportedContentType {
        MediaType anyMediaType = APPLICATION_JSON;

        assert !SUPPORTED_TYPES.containsKey(anyMediaType);
        new MagicNumberCandidate(anyMediaType, new byte[0]);
    }

    @Test(expected = UnsupportedContentType.class)
    public void failsWhenTypeAndContentDoNotMatch() throws UnsupportedContentType {
        MediaType png = IMAGE_PNG;
        byte[] jpegHeader = SUPPORTED_TYPES.get(IMAGE_JPEG);

        new MagicNumberCandidate(png, jpegHeader);
    }

    @Test
    public void canRetrieveContentAndTypeFromCandidate() throws UnsupportedContentType {
        MediaType jpeg = IMAGE_JPEG;
        byte[] jpegHeader = SUPPORTED_TYPES.get(jpeg);

        MagicNumberCandidate candidate = new MagicNumberCandidate(jpeg, jpegHeader);
        assertEquals(jpeg, candidate.getType());
        assertEquals(jpegHeader, candidate.getContent());
    }
}
