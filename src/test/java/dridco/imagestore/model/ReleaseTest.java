package dridco.imagestore.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;

import static dridco.imagestore.model.ClientBuilder.aClient;
import static dridco.imagestore.model.Dates.parse;
import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReleaseTest {
	
	private Client requester = aClient().withDefaults().build();
	@Mock
	private Identifiers identifiers;
	@Mock
	private Images images;
	@Mock
	private StorageEventListener listener;
	private Date now;
	private Command<Boolean> tested;

	@Before
	public void setUp() throws ParseException {
		now = parse("2014-09-26 10:38:31");
		tested = new CommandFactory(listener).release(requester, ImageBuilder.DEFAULT_ID, new EntityTagsRequestParameter(singletonList("\"" + ImageBuilder.DEFAULT_HASH + "\"")));
	}

	@Test
	public void thatTheStorageTriggersAnEventWhenReleasingAnIdentifierThatIsStillLinked() throws ImageStoreException {
		Identifier identifier = anIdentifier().withDefaults().ownedBy(requester).build();
		when(identifiers.get(anyString())).thenReturn(identifier);
		when(images.get(identifier)).thenReturn(ImageRequests.image());
		when(identifiers.remove(Mockito.<Identifier> any())).thenReturn(new StillLinked());

		tested.execute(identifiers, images, now);

		verify(listener).released(Mockito.<Identifier> any());
		verifyNoMoreInteractions(listener);
	}

	@Test
	public void thatTheStorageTriggersImageRemovalEventWhenTheReleasedIdentifierIsTheLastOne() throws ImageStoreException {
		Identifier identifier = anIdentifier().withDefaults().ownedBy(requester).build();
		when(identifiers.get(anyString())).thenReturn(identifier);
		when(images.get(identifier)).thenReturn(ImageRequests.image());
		when(identifiers.remove(Mockito.<Identifier> any())).thenReturn(new Orphaned(identifier));

		tested.execute(identifiers, images, now);

		verify(listener).released(Mockito.<Identifier> any());
		verify(listener).removed(Mockito.<Image> any());
	}
}
