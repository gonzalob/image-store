package dridco.imagestore.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.util.Date;

import static dridco.imagestore.model.Dates.parse;
import static dridco.imagestore.model.IdentifierBuilder.anIdentifier;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GetTest {

	private static final String ID = IdentifierBuilder.DEFAULT_ID;

	@Mock
	private Identifiers identifiers;
	@Mock
	private Images images;
	@Mock
	private StorageEventListener listener;
	private Date now;
	private Get tested;
	
	@Before
	public void setUp() throws ParseException {
		tested = new CommandFactory(listener).get(ID);
		now = parse("2014-09-26 10:38:53");
	}

	@Test(expected = IdentifierDoesNotExist.class)
	public void thatNothingHappensWithImagesIfTheIdentifierDoesNotExist() throws ImageStoreException {
		when(identifiers.get(anyString())).thenThrow(new IdentifierDoesNotExist());

		try {
			tested.execute(identifiers, images, now);
		} finally {
			verifyZeroInteractions(images);
		}
	}

	@Test
	public void thatTheStorageIsQueriedWithTheLoadedIdentifier() throws ImageStoreException {
		Identifier identifier = anIdentifier().withDefaults().build();

		when(identifiers.get(ID)).thenReturn(identifier);

		tested.execute(identifiers, images, now);

		verify(images).get(identifier);
	}

	@Test
	public void thatItTriggersAnEventWhenGettingAnImage() throws ImageStoreException {
		tested.execute(identifiers, images, now);
		verify(listener).loaded(Mockito.<Image> any());
	}
}
