package dridco.imagestore.model.gcs;

import com.google.cloud.storage.testing.RemoteStorageHelper;
import dridco.imagestore.model.Images;
import dridco.imagestore.model.ImagesTest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.google.api.client.util.Throwables.propagate;
import static org.apache.commons.codec.binary.Base64.decodeBase64;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.Assume.assumeTrue;

/**
 * This class requires credentials to access Google Cloud Storage, and an empty bucket to
 * work with.
 * <p>
 * Provide these on runtime, by setting the {@link #GOOGLE_CLOUD_PROJECT_PROPERTY},
 * {@link #GOOGLE_CLOUD_BUCKET_PROPERTY} and {@link #GOOGLE_CLOUD_ENCODED_KEY_PROPERTY} system properties
 * <p>
 * <ul>
 * <li>{@link #GOOGLE_CLOUD_ENCODED_KEY_PROPERTY} expects a json key from Google, encoded in Base64</li>
 * <li>{@link #GOOGLE_CLOUD_PROJECT_PROPERTY} must be the project's identifier</li>
 * <li>{@link #GOOGLE_CLOUD_BUCKET_PROPERTY} must be the name of a bucket. this bucket will be emptied
 * several times, so make sure it is an empty one to begin with</li>
 * </ul>
 * <p>
 * If any of these properties is missing, the test suite will be ignored.
 */
public class GoogleCloudStorageImagesTest extends ImagesTest {

    private static final String GOOGLE_CLOUD_PROJECT_PROPERTY = "test.gcs.project";
    private static final String GOOGLE_CLOUD_BUCKET_PROPERTY = "test.gcs.bucket";
    private static final String GOOGLE_CLOUD_ENCODED_KEY_PROPERTY = "test.gcs.base64-encoded-key";

    @Override
    protected Images create() {
        String project = System.getProperty(GOOGLE_CLOUD_PROJECT_PROPERTY);
        String bucket = System.getProperty(GOOGLE_CLOUD_BUCKET_PROPERTY);
        String base64EncodedKey = System.getProperty(GOOGLE_CLOUD_ENCODED_KEY_PROPERTY);

        assumeTrue(
                "Test disabled, missing configuration. See this class' javadoc for details",
                isNotBlank(bucket) && isNotBlank(bucket) && isNotBlank(base64EncodedKey)
        );

        try (InputStream key = new ByteArrayInputStream(decodeBase64(base64EncodedKey))) {
            GoogleCloudStorageImages images = new GoogleCloudStorageImages(
                    RemoteStorageHelper.create(project, key).getOptions(),
                    bucket
            );
            images.clear();
            return images;
        } catch (IOException e) {
            throw propagate(e);
        }
    }
}
