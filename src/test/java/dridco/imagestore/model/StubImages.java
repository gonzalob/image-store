package dridco.imagestore.model;

public class StubImages implements Images {

	private boolean removeInvoked = false;

	@Override
	public Image get(Identifier identifier) {
		throw notImplemented();
	}

	@Override
	public boolean store(RawImage created) {
		throw notImplemented();
	}

	@Override
	public void remove(String hash) {
		removeInvoked = true;
	}

	public boolean removeInvoked() {
		return removeInvoked;
	}

	private UnsupportedOperationException notImplemented() {
		throw new UnsupportedOperationException("not implemented");
	}

	@Override
	public Integer count() {
		throw notImplemented();
	}
}
