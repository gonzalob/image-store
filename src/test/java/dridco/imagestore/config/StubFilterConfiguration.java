package dridco.imagestore.config;

import dridco.imagestore.model.FilterRegistry;
import dridco.imagestore.model.FilterRegistryBuilder;
import dridco.imagestore.model.filters.StubFilterFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StubFilterConfiguration {

	public static final String FILTER_KEY = "s";
	public static final String EXPECTED_CONFIG = "applied";
	public static final String SPLITTER = "#";

	@Autowired
	@Bean
	public FilterRegistry registry(StubFilterFactory filterFactory) {
		return new FilterRegistryBuilder()
			.withNameConfigSplitter(SPLITTER)
			.addFilter(FILTER_KEY, filterFactory).build();
	}

	@Bean
	public StubFilterFactory stubFilter() {
		return new StubFilterFactory(EXPECTED_CONFIG);
	}
}
