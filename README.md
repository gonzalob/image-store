# Welcome

The Image Store project aims at providing efficient storage and serving of image resources.

# Development

The project's source code, issue tracking and documentation is all hosted at [Bitbucket](https://bit.ly/image-store).

# Building

This is a [Gradle](https://www.gradle.org) project, with its Gradle Wrapper setup. Compiling the project 
should be a matter of running `gradlew build` at the root of the source tree.

# Requirements

The only requirement prior to invoking Gradle is to have Java 1.7 or greater installed in the system.
Linux users should be able to find a package for it. Users of other platforms are encouraged to check [Oracle's JDK](https://bit.ly/1yjcFfn) page.

# Licensing

Please read the LICENSE file.

